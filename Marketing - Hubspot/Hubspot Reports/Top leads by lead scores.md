
Say you had created score properties and your contacts are scored by automation. Now you want a report of your top leads by lead scores:

![](HnClffy.png)

Creating a report or dashboard in HubSpot to showcase the top scores of your contacts involves a few steps focused on leveraging HubSpot’s reporting tools. Here’s how you can create such a report:

1. **Access the Reports:**
- From your HubSpot dashboard, navigate to **Reports** > **Reports**.
- Click on **Create custom report** from the top right corner.

3. **Select the Report Type:**
- Choose **Contacts** as the primary data set for your report.
- This selection is crucial as it allows you to access contact properties, including the Score field.

5. **Configure the Report:**
- Drag and drop the fields you want to include in your report. Make sure to include the **Score** property.
- You can also add other relevant fields such as contact name, email, company name, last activity date, etc., to provide more context to the scores.

7. **Sort and Filter Your Data:**
- To specifically target top scores, you need to sort your report by the Score property. Choose to sort from highest to lowest to get the top scores at the top of your report.
- You can also apply filters to refine the data further, such as scores above a certain threshold or scores updated within a specific timeframe.

9. **Visualizing the Data:**
- Choose how you want to visualize your data; you might select a table for a straightforward list or a bar chart to compare scores visually.
- Configure the visualization options to best display the data you need.

11. **Save and Add to Dashboard:**
- Once you are satisfied with your report, click on **Save**.
- Give your report a name and, optionally, add it to an existing dashboard or create a new dashboard. This makes it easier to access and review regularly.

13. **Share and Collaborate:**
- Share your dashboard with team members who can benefit from viewing this data. HubSpot allows you to set permissions and sharing options to manage who can view or edit your reports.

By following these steps, you'll be able to effectively track and report on the top scores among your contacts, enabling your team to prioritize and engage with your most promising leads more efficiently.