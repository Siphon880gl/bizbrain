Clicking “Sample reports” at the top will show you specific combinations that are best practice

![](WJLj64J.png)

---

See sample reports in action

Easiest one if you dont have much data is “Who owns the contacts created this month?” Make sure CRM → Contacts have ownership set to anyone (maybe you). The sample contacts dont have any contact ownership so you have to edit their contact ownership to you by clicking each sample contact and editing the Contact Ownership.

Generates:
![](Ajzh9Hs.png)
