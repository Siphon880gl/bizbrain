
Requires:
- Sales Hub - Enterprise
- Service Hub - Enterprise


"HubSpot UI extensions enable you to customize HubSpot’s CRM record UI to suit your needs. UI extensions can interact with both HubSpot and external data so that you can quickly access data from within and outside of the account."
https://developers.hubspot.com/docs/platform/ui-extensions-overview


---

Serverless function as backend: 
- myProject -> src -> app -> app.functions.

React as frontend: 
- myProject -> src -> app -> extensions