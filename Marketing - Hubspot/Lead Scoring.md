
Settings (gear icon) → Data Management: Properties
Add a property. At second step, select field type Score

Yes, after you add a new field type of Score in Data Management's Properties in HubSpot, you can create contacts in the CRM and assign them this property. 

Automating Score Assignments:
- Consider setting up workflows to automate the scoring process based on contact interactions or behaviors. This can be done under Automation > Workflows in HubSpot.
- Use triggers such as email interactions, page views, form submissions, etc., to automatically update the Score property.