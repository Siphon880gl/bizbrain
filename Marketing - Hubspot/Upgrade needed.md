
Some features that require a higher plan (like enterprise when you're at professional), they will show with an up arrow:
![](2agLN0a.png)


Some features that are at a higher tier just don't show up at all. See HubDB is missing this time:
![](wMbsUwk.png)
