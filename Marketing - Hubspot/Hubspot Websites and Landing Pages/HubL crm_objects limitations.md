cannot show all properties

For example, this works:
```
crm_objects("contact", "firstname=Bob&order=lastname&order=createdate") 
```

But when you check for the properties hs_lead_status,hubspot_owner_id,hs_marketable_status, they wont appear. There simply wont be any errors. For all properties you can access, you should use Hubspot API