
This will search for specific contact with email contact@company.com and display their first and last name:

```
{% set contact = crm_object("contact", "email=contact@company.com", "firstname,lastname", false) %}
```

This will search by ID:
```
{% set contact = crm_object("contact", 123) %}
{{ contact.firstname }}
{{ contact.lastname }}
```

From: https://developers.hubspot.com/docs/cms/hubl/functions#crm-objects