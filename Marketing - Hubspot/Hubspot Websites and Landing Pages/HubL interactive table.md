
This is an example of rendering HubDB custom table, but more importantly, making it interactive with headers you can click to sort by that column and a search box to filter the rows

Because Hubspot CMS allows for style blocks, script blocks, remote scripts, remote stylesheets, etc we can bring in DataTables and its dependency jQuery. This automatically makes `<table>` syntax enriched with user interactivity

```
<!--  
  templateType: page  
  isAvailableForNewContent: true  
  label: Blank  
  screenshotPath: ../images/template-previews/blank.png  
-->  
{% extends "./layouts/base.html" %}  
  
{% block body %}  
  
{#  
{% dnd_area "dnd_area"  
  label="Main section"  
%}  
{% end_dnd_area %}  
  
#}  
  
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">  
    <link href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" rel="stylesheet">  
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>  
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>  
    <style>  
        .dataTables_wrapper select,  
        .dataTables_wrapper .dataTables_filter input {  
            appearance: none;  
            border: 1px solid #ddd;  
            padding: 0.5rem;  
            margin: 0.5rem;  
        }  
    </style>  
  
    {% set all_rows = hubdb_table_rows(18762756) %}  
    <div class="container mx-auto mt-5">  
        <h1 class="text-3xl">  
          Tasks Table  
        </h1>  
        <br/>  
  
        <table id="contacts_x" class="table-auto w-full text-left">  
            <thead>  
                <tr>  
                    <th class="px-4 py-2">Full Name</th>  
                    <th class="px-4 py-2">Email</th>  
                    <th class="px-4 py-2">Task</th>  
                    <th class="px-4 py-2">Timestamp</th>  
                </tr>  
            </thead>  
            <tbody>  
              {% if all_rows %}  
                {% for row in all_rows %}  
                <tr>  
                    <td class="border px-4 py-2">{{ row.full_name }}</td>  
                    <td class="border px-4 py-2">{{ row.email }}</td>  
                    <td class="border px-4 py-2">{{ row.task }}</td>  
                    <td class="border px-4 py-2">{{ row.updatedAt|datetimeformat('%Y-%m-%d %H:%M:%S') }}</td>  
                </tr>  
               {% endfor %}  
            </tbody>  
        </table>  
        
        <script>  
        $(document).ready(function(  
) {  
            $('#contacts_x').DataTable({  
                "paging": true,  
                "searching": true,  
                "ordering": true,  
                "info": true,  
                "autoWidth": false  
            });  
        });  
        </script>  
         {% else %}  
           <p class="text-bold">No signals information available.<br/></p>   
         {% endif %}  
    </div>  
  
  
{% endblock body %}
```