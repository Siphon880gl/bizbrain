
You may see HubL like:
```
  {% include_dnd_partial  
    path="../sections/contact-full.html"  
    context={  
      "anchor_id": "contact",  
      "background_color": generated_content.colors.accent_background,  
      "heading": generated_content.page_content.contact.heading,  
      "content": generated_content.page_content.contact.content,  
      "form": generated_content.page_content.contact.form,  
    }  
  %}
```

Notice the path. It could correspond to:
@hubspot → growth → sections → contact-full.html

That's where the HubL is loaded as a partial