
When asking ChatGPT for HubL code, it's outdated. 5/16/24.

For example, it’d say `row.values.name`  for HubDB or `contact.properties.name`  for CRM contact even if you correct it. Sometimes it says HubL has no access to CRM standard objects like contacts. The newer Hubspot simplified the variables to `row.name`  and `contact.name`