```


{% set all_rows = hubdb_table_rows(18496005) %}
<div class="contact-info-list">
  {% if all_rows %}
    {% for row in all_rows %}
      <div class="contact-info">
        <p>Name: {{ row.name }}</p>
        <p>Source: {{ row.source }}</p>
      </div>
    {% endfor %}
  {% else %}
    <p>No contact information available.</p>
  {% endif %}
</div>

```


Edit template you can use `<script>` blocks