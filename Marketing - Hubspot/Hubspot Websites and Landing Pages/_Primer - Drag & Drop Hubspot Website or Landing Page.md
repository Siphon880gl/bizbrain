You create a website or landing page like this:

Content → Website / Landing Page

Then there are two major steps:

- 1. Select theme
- 2. Select template (eg.

However Hubspot tries to be predictive based on previous website creations. So you may be at the Select theme step or it may skip to Select template step when you create a website/landing page

If select theme, you can select from Marketplace or Hubspot theme.

If select template and you want to to select theme, go Theme → Change Theme

![](lfdRc0m.png)

When you select a template, it lets you select eg. Blank, or About, or Contact, that the webpage you're designing will be based off.