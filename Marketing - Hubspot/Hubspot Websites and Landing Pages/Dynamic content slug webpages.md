

We can have url like [https://1111111111.hs-sites.com/en-us/contact/51] which can give you information about the contact with record id 51


Settings -> Template. Under Dynamic pages, you can select data source and dynamic page slug. Respectively, Contact, record ID


Then set Dynamic page title to First Name, then [https://1111111111.hs-sites.com/en-us/contact/51](https://1111111111.hs-sites.com/en-us/contact/51) that will give you a page with the tab title of the first name if you have a contact with record id 51

Go to CRM -> Contact then you may need to re-enable record id column. Use that ID for the url.