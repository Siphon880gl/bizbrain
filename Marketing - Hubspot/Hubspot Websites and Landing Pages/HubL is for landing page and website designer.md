
HubL works for landing page and website designer


To edit the entire webpage in HubL templating code:
Settings -> Template -> Edit this template


At the Landing Page or Website Page editors, you can copy sections as HubL templating code:
- Add `?developerMode=true` to the end of the editor’s URL
- Then you can copy that to Settings -> Template -> Edit this template


