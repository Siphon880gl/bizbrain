## Setup

Sometimes drag and drop isn’t enough. You need to edit code to bring in other code snippets or to incorporate javascript (for API calls, etc).

If you want to edit a page’s code, remember the distinction between Theme and Template
Theme is a combination of templates (blank, about, contact, etc) and its styling rules.

You start editing by going to:
Settings -> Template -> Edit this template
![](3WRaW5k.png)

However you might not be able to edit the HubL code
![](CHS5cFz.png)

You’d have to “Create child theme”. Name it a different name than the theme:
![](pxwwovi.png)

Here the theme was growth and I rename the theme child as “growth-child”

Next screen lets you edit right away, however you might be editing the wrong template.You have to edit the correct template for the webpage you were editing on the drag and drop window. Notice the settings page for Template from a prior step:
![](lDw0FLR.png)

If you need to edit another template (like About, Contact, etc):

Click “Back to all files” so you’re at root
![](OI8SY01.png)

At the root, after collapsing the folders you may see:
![](Ysck7bw.png)


In this example, the theme child is growth-child and the parent original theme is at @hubspot/growth

Try to switch to a child theme’s template that you need to edit:

Notice we don’t even have the About template that we need to edit in order to reflect our code changes to the drag and drop window:

![](HNgCDtH.png)
  
And this is because after duplicating the theme as a child theme, it duplicates the bare minimum and you have to separately duplicate templates that you will be editing:

Here I go into @hubspot/growth parent theme, then open into templates. I right-click about.html in order to clone into the child theme files:

![](mYdnviU.png)

Notice that the child theme folder now has the template file that corresponds to your webpage’s settings - about.html:
![](QEfB7AB.png)

Now I can edit the template in the child theme’s folder, and my code changes will be reflect on the webpage that uses that template.

If your changes are not going through, you may need to go to Settings -> Template -> Use different template. Then select the child theme, then select the page template, eg. About or Blank. THEN, publish from the drag and drop editor. Now code changes will be reflect on the webpage that uses that template. You can confirm you're on the child theme by going to (instead of Settings) Edit and its submenu shows the child theme name:

![](jfCrPdp.png)


---

## Example of code change.

Notice 123456 is not inside a dnd_area and remains inside a body. It is not after an end body tag. Make sure to publish the template from the code editor.

```
<!--
  templateType: page
  isAvailableForNewContent: true
  label: Blank
  screenshotPath: ../images/template-previews/blank.png
-->
{% extends "./layouts/base.html" %}

{% block body %}
{% dnd_area "dnd_area"
  label="Main section"
%}
{% end_dnd_area %}

123456

{% endblock body %}

```

---

## Get rid of footer/header? 


Edit `child/templates/layouts/base.html` . Remember your drag and drop editor needs to set the theme to your child theme because you can't edit code in Hubspot themes.
  

Like this:
Notice commented out header and footer. The standard_footer_includes is left alone because it plays a role in Hubspot analytics.
```
<!--
  templateType: none
-->
<!doctype html>

{# Retrieve translations for the template #}
{% set template_translations = load_translations('../_locales', html_lang, 'en') %}

<html lang="{{ html_lang }}" {{ html_lang_dir }}>
  <head>
    <meta charset="utf-8">
    {% if page_meta.html_title %}
      <title>{{ page_meta.html_title }}</title>
    {% endif %}
    {% if branding_favicon %}
      <link rel="shortcut icon" href="{{ branding_favicon }}" />
    {% endif %}
    <meta name="description" content="{{ page_meta.meta_description }}">
    {{ require_css(get_asset_url("../../css/main.css")) }}
    {# This is intended to be used if a template requires template specific style sheets #}
    {% if template_css %}
      {{ require_css(get_asset_url(template_css)) }}
    {% endif %}
    {{ require_css(get_asset_url("../../css/theme-overrides.css")) }}
    {# To see a full list of what is included via standard_header_includes please reference this article: https://developers.hubspot.com/docs/cms/hubl/variables#required-page-template-variables #}
    {{ require_js(get_asset_url("../../child.js")) }}
{{ standard_header_includes }}
{{ require_css(get_public_template_url("../../child.css")) }}
  </head>
  <body>
    <div class="body-wrapper {{ builtin_body_classes }}">
      
      {#
      {% block header %}
        {% global_partial path="../partials/header.html" %}
      {% endblock header %}
      #
      }
      {# The main-content ID is used for the navigation skipper in the header.html file #}
      <main id="main-content">
        {% block body %}
        {% endblock body %}
      </main>

      {#
      {% block footer %}
        {% global_partial path="../partials/footer.html" %}
      {% endblock footer %}
      
      #}
    </div>
    
    
    
    {# This is intended to be used if a template requires template specific JavaScript files #}
    {% if template_js %}
      {{ require_js(get_asset_url(template_js)) }}
    {% endif %}
    
    
    {# To see a full list of what is included via standard_footer_includes please reference this article: https://developers.hubspot.com/docs/cms/hubl/variables#required-page-template-variables #}    
    {{ standard_footer_includes }}
    
  </body>
</html>

```