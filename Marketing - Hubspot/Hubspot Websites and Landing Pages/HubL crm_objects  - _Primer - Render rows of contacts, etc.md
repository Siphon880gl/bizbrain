Contacts etc which are standard objects

```
<h2 class="text-2xl">Contacts (Standard Objects)</h2>  
        {% set contacts = crm_objects("contact", "limit=10") %}  
<!--         <div class="text-sm">Total Contacts: {{ contacts.total }}</div> -->  
        {% if contacts.total %}  
        <table id="contacts_x" class="table-auto w-full text-left">  
            <thead>  
                <tr>  
                    <th class="px-4 py-2">Email</th>  
                    <th class="px-4 py-2">First Name</th>  
                    <th class="px-4 py-2">Last Name</th>  
                    <th class="px-4 py-2">Lead Status</th>  
                    <th class="px-4 py-2">Marketing Contact Status</th>  
                    <th class="px-4 py-2">Contact Owner</th>  
                    <th class="px-4 py-2">Date</th>  
                </tr>  
            </thead>  
            <tbody>  
                {% for contact in contacts.results %}  
                <pre>{{ contact }}</pre>  
                <tr>  
                    <td class="border px-4 py-2">{{ contact.email }}</td>  
                    <td class="border px-4 py-2">{{ contact.firstname }}</td>  
                    <td class="border px-4 py-2">{{ contact.lastname }}</td>  
                    <td class="border px-4 py-2">{{ contact.hs_lead_status }}</td>  
                    <td class="border px-4 py-2">{{ contact.marketing_contact_status }}</td>  
                    <td class="border px-4 py-2">{{ contact.hs_contact_owner }}</td>  
                    <td class="border px-4 py-2">{{ contact.createdate }}</td>  
                </tr>  
                {% endfor %}  
            </tbody>  
        </table>
```

---


If you output each contact instead of each property, your example output could be:
```
{firstname=Maria, createdate=3/18/24, id=1, email=emailmaria@hubspot.com, lastname=Johnson (Sample Contact)}  
{firstname=Brian, createdate=3/18/24, id=51, email=bh@hubspot.com, lastname=Halligan (Sample Contact)}  
{firstname=Ethan, createdate=5/8/24, id=19693403216, email=ethan.morris@example.com, lastname=Morris}  
{firstname=Olivia, createdate=5/8/24, id=19693404844, email=olivia.thompson@example.com, lastname=Thompson}  
{firstname=Liam, createdate=5/8/24, id=19697904384, lastname=Jenkins}  
{firstname=Marco, createdate=5/8/24, id=19697904704, email=marco.rossi@example.com, lastname=Rossi}  
{firstname=Theo, createdate=5/8/24, id=19697905072, email=theo.wallace@example.com, lastname=Wallace}  
{firstname=Victor, createdate=5/8/24, id=19697905184, email=victor.chen@example.com, lastname=Chen}  
{firstname=Ava, createdate=5/8/24, id=19697905502, email=ava.kim@example.com, lastname=Kim}
```