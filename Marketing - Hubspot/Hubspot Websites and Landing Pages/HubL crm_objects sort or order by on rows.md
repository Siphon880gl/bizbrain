
That is:
```
crm_objects("contact", "firstname=Bob&order=lastname")
```

And you can also sort by two properties:
```
crm_objects("contact", "firstname=Bob&order=lastname&order=createdate")
```

To reverse a sort, prepend `-` to the property name like `order=-createdate` 