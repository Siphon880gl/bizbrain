When you create a Hubspot website or landing page, you get to select a Theme.

That Theme contains templates which are individual pages like About, Blank, Pricing, Product

So when creating a website/landing page, it might ask you to choose a theme, then ask you to choose a template (the webpage)

The webpage will be wrapped between header and footer, very likely. This is because there's a base layout that contains header, your template, and footer. The template part of the base layout is dynamic. 

You can see this by editing the HubL code going into Settings -> Template -> Edit. @hubspot has the theme folder. Inside theme folder is a templates folder containing Blank, About, etc. The templates folder also contains layout/base.html

If you want to modify the webpage code, which is either Blank, About, Product, Price, etc based on what your webpage is based on, keep in mind which template it is

Template filename is shown at Settings -> Edit this template. See here it's "blank.html"

![](XRczwrR.png)

When you try to modify it might warn you cannot modify Hubspot original files.

You need to perform these steps:
1. Clone the theme as a child theme
2. Change the website to the child theme (you had named it). See "use different template":
   
![](XRczwrR.png)

And you can confirm the theme change at Edit ->
![](EgIn4Is.png)

3. Then you need to clone the template or page into the child theme (this is because when you cloned the theme as a child theme, it cloned lazily and did not copy over all files):
![](DyQNQBU.png)

4. Open the child theme's file to edit (not stay in the parent theme's file)
   