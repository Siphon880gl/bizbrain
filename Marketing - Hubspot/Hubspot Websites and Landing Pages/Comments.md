
In HubL (HubSpot Markup Language), you can add comments to your code that will not be rendered in the final output. This is useful for adding notes or explanations within your templates and scripts, making the code easier to understand and maintain.

Here’s how you can add comments in HubL:

### Single-Line Comments
For single-line comments, you use the `{# #}` syntax. Anything you place between `{#` and `#}` will be considered a comment and ignored when the page is rendered.

```html
{# This is a single-line comment in HubL #}
```

### Multi-Line Comments
The same syntax is used for multi-line comments. You simply extend the comment across multiple lines like this:

```html
{# 
  This is a multi-line comment in HubL
  It can span several lines
#}
```

### Usage in Code
Here's an example of how comments might be used within a typical HubL template:

```html
{# Main header module #}
{% module "header_module" label="Site Header" path="global/header" %}

{# Main content area #}
{% dnd_area "main_content" %}
  {% dnd_section %}
    {% dnd_row %}
      {% dnd_column %}
        {# Insert rich text module here #}
        {% module "rich_text" path="custom/rich_text_module" label="Main Content" %}
      {% end_dnd_column %}
    {% end_dnd_row %}
  {% end_dnd_section %}
{% end_dnd_area %}

{# Footer module reference #}
{% module "footer_module" label="Site Footer" path="global/footer" %}
```

Using comments like these helps clarify the purpose of different sections and modules in your HubL templates, which can be especially helpful when working in teams or when returning to a project after some time.
