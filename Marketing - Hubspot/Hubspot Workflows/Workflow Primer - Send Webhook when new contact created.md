
Choose a type of workflow, eg. Contact-based → Blank workflow

Choose a trigger → Event → Object created (search and click)

Choose a trigger, for example Go to action → Data ops → Send a webhook (You can include all contact properties under request body, then after looking at the json response, you can decide to have custom request body)

Create a php endpoint on your own custom server, eg. wengindustry.com/api/hubspot/hooks/
This example php script saves to a log file:
```
<?php  
// Enable error reporting for debugging  
ini_set('display_errors', 1);  
ini_set('display_startup_errors', 1);  
error_reporting(E_ALL);  
  
// Function to log request body  
function log_request_body($file_path, $error_log_path) {  
    // Get the request body  
    $request_body = file_get_contents('php://input');  
      
    // Check if the request body is empty  
    if (empty($request_body)) {  
        $response = [  
            "handshake" => "error",  
            "message" => "No request body sent. Wrong workflow? Visited URL directly? Meant to be for a Contact object creation webhook"  
        ];  
        log_error($error_log_path, $response['message']);  
        echo json_encode($response);  
        return;  
    } else {  
        // Convert newlines to HTML <br/> tags  
        $request_body_html = nl2br(htmlspecialchars($request_body));  
  
        // If just plain raw dump:  
        // $log_entry = "<p>" . date('Y-m-d H:i:s') . "<br/>" . $request_body_html . "</p><br/>";  
  
        // Decode JSON request body  
        $data = json_decode($request_body, true);  
  
        // Extract key properties  
        $log_data = [  
            'vid' => $data['vid'] ?? 'N/A',  
            'firstname' => $data['properties']['firstname']['value'] ?? 'N/A',  
            'lastname' => $data['properties']['lastname']['value'] ?? 'N/A',  
            'email' => $data['properties']['email']['value'] ?? 'N/A',  
            'createdate' => isset($data['properties']['createdate']['value']) ? date('Y-m-d H:i:s', $data['properties']['createdate']['value'] / 1000) : 'N/A'  
        ];  
  
        // Create a log entry  
        $log_entry = "<p>" . date('Y-m-d H:i:s') . "<br/>" .   
                    "VID: " . htmlspecialchars($log_data['vid']) . "<br/>" .   
                    "First Name: " . htmlspecialchars($log_data['firstname']) . "<br/>" .   
                    "Last Name: " . htmlspecialchars($log_data['lastname']) . "<br/>" .   
                    "Email: " . htmlspecialchars($log_data['email']) . "<br/>" .   
                    "Create Date: " . htmlspecialchars($log_data['createdate']) . "</p><br/>";  
          
  
        // Append the log entry to the file  
        file_put_contents($file_path, $log_entry, FILE_APPEND);  
          
        $response = [  
            "handshake" => "success",  
            "message" => "New contact information sent to hooks/log"  
        ];  
        echo json_encode($response);  
        return;  
    }  
}  
  
// Function to log errors  
function log_error($error_log_path, $error_message) {  
    $error_entry = "<p>" . date('Y-m-d H:i:s') . "<br/>" . htmlspecialchars($error_message) . "</p><br/>";  
    file_put_contents($error_log_path, $error_entry, FILE_APPEND);  
}  
  
// Specify the log file paths  
$log_file_path = 'request_log.html';  
$error_log_path = 'error_log.html';  
  
// Set content type to JSON  
header('Content-Type: application/json');  
  
// Call the function to log the request body  
log_request_body($log_file_path, $error_log_path);  
  
?>
```


Turn on your workflow (Review and Publish at top right of workflow designer). Btw clicking test button at the top right won't connect to your webhook, so go ahead and publish it. When going to list of workflows, you should see ON for the status.

The PHP script example appends to: request_log.html

So to test this works, manually create a contact at CRM -> Contacts. In a few seconds, visiting request_log.html should show you the new contact information. This proves that whenever a new contact is created in Hubspot, automatically or manually like we just did, that info sends over to your server. From here you can have your own database, custom dashboard, or more data analytics. You can create tools that people pay in a SaaS model that integrates contacts, etc to your tool

If this fails to log to request_log.html, you need to debug.

---

## Debug

See PHP errors and PHP outputs here

Automation -> Workflow -> More:View Details on your workflow -> Enrollment history tab

Then under “Webhook executed using POST method” you can see any PHP errors

![](n5n1Ii4.png)

In this example it’s permission denied so you would have to fix permission issues with files. Briefly, you go on SSH of your private server, then chmod and chown to the same user and group with write permission that your script acts as when it's visited online. That is outside the scope of this tutorial. Refer to Weng's Development notes.

