
Hubspot likes to call the tracking code an: embed code

Could take 24 hours before data appears. 

You CANNOT add specific paths like yourdomain.com/subpath, only the domain like yourdomain.com

You can have tracking ignore traffic to your domain that was referred by certain domains

You CANNOT validate the tracking easily within Hubspot, but you can inspect to check the js file is working
https://knowledge.hubspot.com/reports/how-do-i-know-if-my-hubspot-tracking-code-is-working

It's hard to find where to get the code. It's at:
Reporting & Data -> Integrations -> Tracking & Analytics -> Tracking Code