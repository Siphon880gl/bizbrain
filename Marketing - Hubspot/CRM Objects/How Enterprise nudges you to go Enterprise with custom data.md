How Hubspot makes you want to go enterprise

Lets say you’re tracking marketing analytics and the standard objects (contacts, companies, tickets, deals) is fine with you. Later on when you want more custom information, you have to create custom objects (requiring CRM enterprise)

If you do not want to pay enterprise and have Content Hub proefssional, you can levearge the custom tables of HubDB. Your API calls from user interactions would update the HubDB. Ok that’s great. So now you can use Content Website or Landpages to create private password protected dashboards that combine your HubDB (in place of custom objects) and CRM/standard objects/contacts via the templating language called HubL. But you will run into an annoying problem although you can solve it with more complicated stacks. Your HubDB can access contact information but will not let you get properties like hs_lead_status and marketing status and contact owner. HubL is limited on CRM data it can retrieve and can retrieve all HubDB information. Now you have two choices, either workflows that combine your contact info with HubDB (theoretically, I haven’t tested if Workflow is limited from accessing some contact properties). The other choice I have tested is you create a backend server to access Hubspot API which has access to more properties from CRM contact, etc standard objects - and you’ll have to set it at your own server because of CORS restrictions.

If you had Enterprise, Reports and Dashboards can be used to bring in standard objects (contacts, etc) AND custom objects. WIth Custom objects which is only enterprise, you are not forced to workaround it using HubDB. And with Reports and Dashboards, you dont have the limitations of HubL templating language only accessing the basic properties of CRM although you could solve it with a javascript call to your own server hosting the Hubspot API calls.

Reworded:
When you dont have Enterprise which easily allows custom data from Dashboards/Reports using Custom Objects, Hubspot doesnt block you entirely but inconveniences you by forcing you to use different techs in order to combine your custom data with standard objects into a table/report/etc
Your own HubL coded dashboard (Not their native Reports and Dashboards)
HubL accesses standard objects’ most basic properties
HubL coded dashboard performs API call to your own backend server that performs Hubspot API calls to get standard objects’ other properties
HubL accesses HubDB (professional tier) which replaces Custom Object (enterprise tier)

The crazy thing is there might be a niche here too for more budget sensitive businesses: Giving enterprise features like custom data reporting without needing to pay for enterprise. But you need to have a talented coder. And when you maintenance tracking and reporting changes, the developer needs to know which tech to affect.

Reworded:
When you dont have Enterprise which easily allows custom data from Dashboards/Reports using Custom Objects, Hubspot doesnt block you entirely

Here I'm using HubDB for the custom tracking which is a workaround for not having Custom Objects (available only on Enterprise), HubL to create a private dashboard with basic properties, and Hubspot API to cover the rest of the properties

![](kgSS50E.png)


And my backend that can feed information back into the dashboard with Hubspot API:
![](pbvvrY0.png)
