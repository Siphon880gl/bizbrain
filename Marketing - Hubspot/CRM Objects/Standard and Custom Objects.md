
Standard objects:
Entities representing the rows you created manually or automatically for contacts, companies, deals, and tickets.

Custom objects:
Entities representing your own custom information with custom properties

Property is firstname, lastname, etc

Hubspot's Reports and Dashboards can combine Standard objects and Custom objects although its data visualization of custom objects are limited due to its natural complexity. Custom objects are only on enterprise which is significantly higher cost than professional, so companies have worked around custom tracking by using HubDB, and Hubspot API that allows them to modify HubDB information, and then on a website combine CRM and HubDB.

