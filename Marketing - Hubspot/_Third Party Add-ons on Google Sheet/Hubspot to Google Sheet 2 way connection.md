
Dokin.co

Sign up with OAuth2 Google account (does not have to be same account you login with Hubspot)

"Connection" tab on the left -> Create new Data (see primary button here)

![](3LTrJET.png)

Hubspot CRM -> From a template -> Create a new Template -> Open Google Sheet (Might ask you for permission) -> Select the spreadsheet (will create a new tab)

Now on this screen, make sure you're on the correct Hubspot account, then select your type of data (eg. Contacts)

![](C9jpMBu.png)

Click Preview:
![](ILzP18j.png)

Customize how the data will look at Google Sheet. Refer here:
![](UX5TdI7.png)

(
The Dokin Header is:
![](5UlN9xg.png)
)

![](lUS3jDX.png)


Then create Template

---

Now at Google Sheet, and install Dokin from Extensions.

After installing, you may need to refresh Google Sheet. The under Extensions menu -> Dokin... -> Start

Here you can refer to the connection templates you connected. It'll sync to your Google sheet 2-way