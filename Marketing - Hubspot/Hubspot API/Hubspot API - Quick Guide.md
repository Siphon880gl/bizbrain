
Reporting & Data → Integration -> Private app -> Create a private app

Make sure to select what CRM etc information the API will have access to. You can always edit these after creation. New hubspot accounts have sample contacts, so that's a good resource to enable.

Once done creating, it'll give you the access token only this ONE time. Make sure to copy it.

Then in terminal, you can test the API works, making sure to adjust YOUR_ACCESS_TOKEN
```
curl --request GET \
  --url 'https://api.hubapi.com/crm/v3/objects/contacts?limit=10&archived=false' \
  --header 'authorization: Bearer YOUR_ACCESS_TOKEN'
```


You should get a result similar to:
```
{"results":[{"id":"1","properties":{"createdate":"2024-03-18T15:38:49.517Z","email":"emailmaria@hubspot.com","firstname":"Maria","hs_object_id":"1","lastmodifieddate":"2024-03-18T15:39:02.091Z","lastname":"Johnson (Sample Contact)"},"createdAt":"2024-03-18T15:38:49.517Z","updatedAt":"2024-03-18T15:39:02.091Z","archived":false},{"id":"51","properties":{"createdate":"2024-03-18T15:38:49.909Z","email":"bh@hubspot.com","firstname":"Brian","hs_object_id":"51","lastmodifieddate":"2024-03-18T15:39:02.452Z","lastname":"Halligan (Sample Contact)"},"createdAt":"2024-03-18T15:38:49.909Z"....
```