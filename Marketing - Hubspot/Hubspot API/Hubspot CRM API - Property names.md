Some property names are just squeezed together like "firstname". Some are snake case with preceded hs like "hs_lead_status". Some are named differently than their column name when you're in Hubspot! For example, marketing contact status’ internal name is hs_marketable_status.

But you need the property name that Hubspot API understands. These are internal names.

You can find out the internal name of properties by going to Settings (gear) → Data Management: Properties.

Search for the property → Edit. Click `</>` code button next to label:

![](Fhw0jng.png)

![](GpJRjua.png)
