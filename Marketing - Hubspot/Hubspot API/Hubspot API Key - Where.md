You get your API key from settings (gear) → Apps → Private Apps → Access Token → Show
Create a private app there if haven't already, before you can see an access token.