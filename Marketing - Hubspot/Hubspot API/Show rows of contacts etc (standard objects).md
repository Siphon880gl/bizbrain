
PHP script that responds with JSON of your contact rows by using Hubspot API:
```
<?php  
$hubspotApiKey = 'XXXX';  // Replace with your actual HubSpot API key  
$contactProperties = 'email,firstname,lastname,hs_lead_status,hubspot_owner_id,hs_marketable_status';  
$limit = 10;  // Number of contacts to fetch  
  
$url = "https://api.hubapi.com/crm/v3/objects/contacts?properties=" . $contactProperties . "&limit=" . $limit;  
// $url = "https://api.hubapi.com/crm/v3/objects/contacts/";  
  
  
$curl = curl_init($url);  
curl_setopt($curl, CURLOPT_HTTPHEADER, array(  
    'Content-Type: application/json',  
    "Authorization: Bearer $hubspotApiKey"  // Set the Authorization header with the Bearer token  
));  
// curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));  
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  
curl_setopt($curl, CURLOPT_TIMEOUT, 30);  
  
$response = curl_exec($curl);  
$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);  
  
if ($status != 200) {  
    echo "Error: Failed to fetch contacts with status code " . $status . "\n";  
} else {  
    echo "Contacts fetched successfully:<br/>";  
    echo $response;  // This will print the raw JSON response  
}  
  
curl_close($curl);  
?>
```

Note that using the Hubspot API on the frontend client will fail due to CORS restrictions. It has to be used backend.

Opening that page could output:
```
Contacts fetched successfully: 
{"results":[{"id":"1","properties":{"createdate":"2024-03-18T15:38:49.517Z","email":"emailmaria@hubspot.com","firstname":"Maria","hs_lead_status":null,"hs_marketable_status":"false","hs_object_id":"1","hubspot_owner_id":null,"lastmodifieddate":"2024-10-12T02:50:48.394Z","lastname":"Johnson (Sample Contact)"},"createdAt":"2024-03-18T15:38:49.517Z","updatedAt":"2024-10-12T02:50:48.394Z","archived":false},{"id":"51","properties":{"createdate":"2024-03-18T15:38:49.909Z","email":"bh@hubspot.com","firstname":"Brian","hs_lead_status":null,"hs_marketable_status":"false","hs_object_id":"51","hubspot_owner_id":null,"lastmodifieddate":"2024-10-12T02:50:45.032Z","lastname":"Halligan (Sample Contact)"},"createdAt":"2024-03-18T15:38:49.909Z","updatedAt":"2024-10-12T02:50:45.032Z","archived":false}}}
```