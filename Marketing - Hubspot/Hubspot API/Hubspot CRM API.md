
Access contact and more properties than the basics (firstname, lastname, email, createddate, id)

You cannot call Hubspot CRM API from client because you’ll be blocked by their CORS policy. So you must do it with a backend

Using NodeJS, node-fetch which is updated to use the new EMACs syntax, you have a

test.mjs:
```
import fetch from 'node-fetch';  
  
var HUBSPOT_API_KEY = '<YOUR_API_KEY>';  // Replace with your actual HubSpot API key  
  
var CONTACT_PROPERTIES = 'email,firstname,lastname,hs_lead_status,hubspot_owner_id,hs_marketable_status';  
  
function fetchContacts() {  
//    var url = `https://api.hubapi.com/crm/v3/objects/contacts?properties=firstname&limit=10&hapikey=${HUBSPOT_API_KEY}`;  
  
var url = `https://api.hubapi.com/crm/v3/objects/contacts?properties=firstname,hs_lead_status,hubspot_owner_id,hs_marketable_status&limit=10`;  
    fetch(url, {  
        headers: {  
          'Authorization': `Bearer ${HUBSPOT_API_KEY}`,  
          'Content-Type': 'application/json'  
        }  
})  
        .then(function(response) {  
            if (!response.ok) {  
                throw new Error(`Failed to fetch contacts: ${response.status} ${response.statusText}`);  
            }  
            return response.json();  
        })  
        .then(function(data) {  
            // console.log('Contacts:', data);  
            data.results.forEach(function(contact) {  
                console.log(contact.properties)  
            });  
              
        })  
        .catch(function(error) {  
            console.error('Error:', error);  
        });  
}  
  
fetchContacts();
```

You get your API key from settings (gear) → Apps → Private Apps → Access Token → Show

Create a private app there if haven't already, before you can see an access token.