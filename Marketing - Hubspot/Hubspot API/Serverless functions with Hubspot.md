

Requirements: Enterprise

why host the serverless function with hubspot? When accessing Hubspot API to get or update data at CRM contact, etc, your API key should be hidden in a backend.

Actually, you could choose to host the backend yourself, then you won't need enterprise / serverless functions with Hubspot.

[https://developers.hubspot.com/docs/cms/data/serverless-functions](https://developers.hubspot.com/docs/cms/data/serverless-functions)

---


Hosting a serverless function on HubSpot involves using the HubSpot Developer Tools to create and deploy serverless functions within your HubSpot account. Here's a step-by-step guide to help you set up and host serverless functions in HubSpot:

### Step 1: Setup Your HubSpot Developer Account
1. **Create a HubSpot Developer Account**: If you don't already have one, you'll need to create a developer account at HubSpot. Visit [HubSpot Developers](https://developers.hubspot.com/) and sign up.

2. **Create an App**: In your developer account, create an app. This is where you can manage your serverless functions and other developer tools.

### Step 2: Set Up Your Local Development Environment
1. **Install Required Tools**: Download and install the HubSpot CLI (Command Line Interface). This tool helps you manage your development projects directly from your terminal or command prompt. You can install it via npm:
   ```bash
   npm install -g @hubspot/cli
   ```

2. **Initialize Your Project**: Create a directory for your project and initialize it with the HubSpot CLI:
   ```bash
   mkdir my-hubspot-project
   cd my-hubspot-project
   hs init
   ```

3. **Connect Your CLI to HubSpot**: Connect the CLI to your HubSpot account by following the prompts after running `hs init`. You will need to provide your account details and get an API key if you don’t already have one.

### Step 3: Create and Develop Your Serverless Function
1. **Create the Function**: Use the HubSpot CLI to create a new serverless function:
   ```bash
   hs create serverless-function my-function
   ```

2. **Develop Your Function**: This will create a folder with a basic function setup, including an `index.js` file where you can write your function code, and a `serverless.json` file which defines the function settings.

3. **Edit Your Function**: Modify `index.js` to add your function logic (e.g., interfacing with the HubSpot API, handling business logic, etc.).

### Step 4: Deploy Your Serverless Function
1. **Upload Your Function**: Deploy your function to HubSpot using the CLI:
   ```bash
   hs upload my-function my-function
   ```

2. **Verify Deployment**: Log into your HubSpot account and navigate to `Menu > Marketing > Files and Templates > Design Tools`. Here you should find your function under the `Serverless` tab. You can manage and monitor it from there.

### Step 5: Test Your Function
- **Invoke the Function**: You can test the function directly from the HubSpot interface or by making HTTP requests to the endpoint URL provided in the Design Tools under the function’s settings.

### Step 6: Monitor and Maintain
- **Logging and Debugging**: HubSpot provides logging tools for serverless functions, accessible from the same `Serverless` tab in the Design Tools. Use these logs to troubleshoot and maintain your functions.

By following these steps, you can successfully create, develop, and deploy serverless functions in HubSpot, allowing you to extend the capabilities of your HubSpot applications and integrate more complex server-side logic securely.