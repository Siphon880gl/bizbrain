
UI extensions available only to Enterprise on Sales/Service, allows you to customize your Hubspot CRM UI

Details:
https://developers.hubspot.com/docs/platform/create-ui-extensions