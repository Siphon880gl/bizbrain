A **bootstrapped startup** is typically a company that is self-funded by its founders rather than relying on external funding from investors or loans. The term "bootstrapping" comes from the idea of "pulling oneself up by one's bootstraps," implying self-reliance and resourcefulness.

Key characteristics of a bootstrapped startup include:

1. **Self-Funding:** The founders use personal savings, revenue from early sales, or small loans from friends and family to get started.
2. **Frugality:** They keep expenses as low as possible, focusing on essential spending. This often involves working out of home offices or coworking spaces, avoiding unnecessary overhead costs, and relying on free or low-cost tools.
3. **Sweat Equity:** The founders and team members often put in a lot of unpaid or underpaid work to build the company.
4. **Revenue-Driven Growth:** The startup relies on generating revenue early on to fund its operations and growth rather than seeking external investment.
5. **Control:** Founders retain full ownership and control over the company since there are no external investors.


Bootstrapping can be a tough journey, but it allows founders to maintain independence and keep a greater share of the equity. Many successful companies, like Mailchimp and Patagonia, started as bootstrapped ventures.