
Google Analytics, Google Postmaster, etc
- Free
- We will need me to be delegated DNS access (if DNS owner and marketing tech person are not the same person).
- And we’ll need a separate google account so we can ALL log in to check analytics

Metabase (Business analytics)
- Free
- Separate accounts for each team member (or all one account)