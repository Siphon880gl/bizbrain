
Situation:
- Multiple partners
- No joint account to pay for company expenses (if you have joint account, then simply divide the expenses between each partner at the end of each month)  
- Multiple paid services for the business (eg. Server costs, APIs, etc) paid by different partners’ own bank accounts  

Not a problem if:
- A joint bank account pays for the multiple services. Then you can just have rows of date, category, item name, debit, credit, similar to:
  ![](Lec001g.png)
  ^ Above from [https://fundsnetservices.com/trial-balance](https://fundsnetservices.com/trial-balance)



But with multiple partners and they pay for services with different partner’s person banking account, follow this guide to follow a fair approach on spreadsheet (With Google Sheet or Excel formulas).

Reworded: Imagine there are three founders who have equal stake in the bootstrapped startup (paying out their own pockets). Because the three don't have a JOINT account, they have to split the cost and one person who paid for hosting, for example, needs to be paid back, yet everyone needs to be made whole by only contributing 1/3 of the costs. This spreadsheet approach outlined here helps reconcile that.

Foreword - This works for multiple partners but let's use the example of 3 partners.

So we know it costs some money to run a business or even bootstrap a business (eg. Server costs, APIs, etc).

With three partners, you want to divide the contributions in three. But because the cost is not from one source (but between multiple banking accounts), it may not be that easy.

The goal is you want to eventually divide the charges evenly, but at the same time, different partners may be billed on their personal bank accounts for different services (often they initiated acquiring the service etc).

So have columns for three partners and for each row which represents a service payment (aka company expense) (aka item name), you write the financial contribution under that partner. You're writing the payment date, the item name, then under that partner you write the contribution amount in positive value (three cells, not counting the Category and Amount, which we will discuss later). Keep the amount positive. The rows are added as payments are made, letting you track the payments chronologically, having newer and newer payments go down the spreadsheet.

![](YNKvtfn.png)

The other partners (debted partners) pay back this partner (contributed partner) because they are in debt to the partner that invested from their own financial bank account (eg. When this partner pays for an API, hosting, etc). The debt payment gets listed on another row; each partner's debt payment needs to be on a different row so we can track chronologically which partner paid back the debt when. Of course, the debted partner needs to pay back evenly divided (so partner A who paid $79 for the month's API will be expecting $26.33 from partner B and $26.33 from partner C). The recordings is done like this: By writing the debt payment date, the item name, how much the debt partner paid in positive amount, and that same amount in negative value under the contributed partner (ignoring the Category and Amount column, which we will discuss later). The negative value let's the contributing partner's contribution be subtracted out because what was owed is now paid.

![](UMk5RUa.png)

^ With the debt payment of other partners to make it fair for the single partner who paid for a service with their own account, next step in the spreadsheet, and naturally further down, we divide the total company expenses by three, and **subtract** that amount at each partner regardless if they overpaid or is underpaid (aka post-subtraction). And under each partner, the positive value is what the partner owes since costs have been tracked with positive values, and the negative value is what that partner is owed. Actually, the three columns should all be zero (1 cent positive/negative off because of natural rounding errors with Gsheet/Excel). If not all zero with margin of error, have the underpaid debted partners (positive value) pay the overpaid respective partner(s) (negative partners). For convincing, they may scroll up to see who contributed from their personal bank account but wasn't paid back and how much to be paid back whole. For more convincing, recall that when someone contributed $79, the other two partners each pay $26.33, and that means everyone paid evenly split, so the total evenly spread subtracting the actual payments under each partner should zero out.

A built-in audit system is to have a column next to the three partners, preferably before them (column G "Amount"), and preferably under-emphasized in style. This column reiterates the cost of that item regardless who paid. This column does NOT reiterate how much debt one partner pays back another partner and is in fact 0 (or leave blank). The sum of this column (at a cell) must match the sum of the three partners' contributions pre subtracting (sum them are another cell), otherwise you made a mistake somehow. Note here in this following example, some rows have been hidden to be concise (9/7/23 jumps to 9/3/24). See column  G which is amount of contribution but does not include paying debt back to the contributing partner (and it would zero out under column G too). See cell L52 which is the sum of Partner A, B, and C on the left (I52-K52). 

![](NtqSULx.png)

At a later date, you can refer to the highlighted cells for auditing. See their numbers match:
![](9joS6rB.png)

---

Formula’s are shown below:

![](lhZ4Kz7.png)

---

Here's a minimalistic example to learn from. Partner A paid for hosting. Then await partner B and Partner C for their contributions (they will pay Partner A $26.33 and $26.33 each):
![](EWlvNuH.png)

^ Note this is a clear slate with no other contributions by anyone. Rows 1-16 were just typical invoice spreadsheet header, just hidden away. This was originally from a Google Sheet template Invoice.

---

## Transaction Fees

If a transaction fee is recurring, it would be unfair to the person who paid with their banking account. The other partners paid this person but there's a transaction fee deducted. This is hard to track on spreadsheet.

So create a folder named "Transaction Fees (Not in Joint Expenses)"

![](O7N0SZ8.png)

TLDR:
- Screenshot file named: 
	- Date of transaction in YYYY.MM.DD sortable form
	- Platform
	- Total
	- Fee
	- Payer names
	- RECURRING/X1
- Example filename: 2025.01.24--Venmo--Total-26.33--Fee-78cents--PARTNER_NAME-X1.png

For transaction fees that occur regularly, take screenshot (eg. Venmo screen) and place screenshot file in that folder. Include date of transaction (screenshot might not be clear on the date). Include both the total amount and the fee in its actual currency value (rather than a percentage), add first names of payers, and name it "RECURRING".  If it helps, you may also add the name of the platform (if using different platforms, eg. venmo, paypal, etc instead of being consistently on the same platform)
- By having it labeled "RECURRING", then you do not have to take a screenshot each time the transaction occurs. When reviewing with team, keep in mind which ones are named RECURRING and trace back the number of transactions to multiply by the fee.

For one-time transaction fees (in the example below, a founder accidentally marked as a good or service), follow the same rules above but write "X1" (rather than "RECURRING")


![](83LsfOo.png)

At the end of each quarter, half-year, or year, review the accumulated transaction fees, since they may be large enough to warrant paying back the partner who contributed with his own banking account.

Those transaction fees that are resolved can be put into a folder "Resolved" inside the Transaction Fees folder.