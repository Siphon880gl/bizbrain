In a bootstrapped tech startup, cofounders should establish clear agreements on compensation for the engineer responsible for building the product. If the startup is not yet profitable, it should be agreed that, once profits are generated, the engineer will receive additional payouts to cover their unpaid hours.

This can create friction if cofounders hesitate to approve features—particularly those that could increase the app's chances of success—fearing it will reduce their payouts. To address this, the engineer might donate time to developing features they believe are absolutely critical, agreeing those hours won’t count toward their compensation. While this can help move the project forward, it carries risks if the donated feature takes the team in a different direction or diverts time from other agreed-upon priorities. Therefore, if you decide to donate hours, it’s crucial to inform the cofounder team and secure their understanding.

Bootstrapping also makes it challenging to attract specialized talent, as the startup may not have the budget to hire experts. If you, as the engineer, need expertise in areas like server management, you might take the initiative to learn and apply these skills yourself. To maintain fairness, you could reduce or waive the hours for such tasks.

These represent two types of donated contributions the bootstrap engineer can make: one for critical features vital to the app's success and another for technical tasks in adjacent fields outside their specialty. While these fields may not be their expertise, they can be learned, allowing the engineer to bridge gaps without adding financial strain to the team. However, as the startup becomes profitable, it may be necessary to hire a specialist to review the code or architecture to ensure quality and scalability. Ensuring the cofounder team understands this plan is essential for long-term alignment and success.

The engineering cofounder may want to bootstrap a spreadsheet. Google Sheets may be a good choice because it can easily be shared among cofounders and it's instantly updated.

You need two tabs of hours tracking:
- Hours
- Donated Hours



Hours:
![](qJUC1Hp.png)


Donated Hours:
![](W2lNPO3.png)

The comment column can highlight that there's a block of tasks for a specific feature.