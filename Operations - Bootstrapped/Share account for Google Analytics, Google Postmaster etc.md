
Share the account so founders/high level decision makers can strategize. Likely have to create a new google account. Unless someone takes the role of marketer directory and presents every month.

Many of these services require TXT verification and may require coordination with the person in the bootstrapped startup that owns the domain, unless that person does both the google marketing implementation and owns the DNS. 

These services can delegate accounts though. For example, Namecheap offers a "Share access".