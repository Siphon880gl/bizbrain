## Spreadsheets


### Microsoft Excel

They have spreadsheets in the software

### Google Sheet

They have spreadsheets on the web version.

Here's how:

1. Create docs and spreadsheets at:
https://docs.google.com/

2. Templates at:
![](8TXz1VI.png)

Some examples:

Team Roster:
![](aOyNwqa.png)


Invoice template:
![](iHeYqoB.png)

Annual Budget, Monthly Budget:
![](sbe1vbq.png)

More Finances, and Project management:
![](HKcHeUF.png)

---

## Other Knowledge Management Tools' Templates

Coda

Notion

Microsoft loop (Notion inspired)

---

## Diagramming Web Apps (Not for data entry but for strategizing etc)

https://mural.co/

Miro

Lucidchart

