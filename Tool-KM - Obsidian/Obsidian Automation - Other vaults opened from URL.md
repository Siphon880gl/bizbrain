You have to create a Vault: File -> OÏpenÏ Vault -> Open folder as vault

A hidden folder .obsidian will be created there.

---

Now you can open directly with:
obsidian://open?vault=Music

In that example, your actual folder path is Users/<Username>/. So it points to Users/<Username>/

Opening obsidian via app link can only be opened to existing vaults (Those with hidden folder .obsidian)