
Treat as if the Youtube link is an image:

```
![](https://www.youtube.com/watch?v=NnTvZWp5Q7o)
```

Or you can paste the `<iframe>` embed code from the Share modal at the Youtube video:

Embed code from youtube:
Note - To reveal code, drag select this line to below the video
<iframe width="560" height="315" src="https://www.youtube.com/embed/NnTvZWp5Q7o?si=rcWocphBEfhex9NW" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>