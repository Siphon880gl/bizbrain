## Image hosting
- imgur no longer recommended. Refer to [[Obsidian - NO Imgur for pasting images no longer recommended]]

## How publish it
- $8 month https://obsidian.md/publish
- Or self host it

## Self Hosting
You will use a combination of git, VPS/dedicated server, SFTP, and node scripts. Alternately, you could use Git hooks as a cleaner solution.

This has been done successfully at https://wengindustries.com/app/devbrain which as of 2/2025 contains almost 2000 notes on programming. I authored the notes in my own local Obsidian and it automatically gets reflected online. The "engine" that I developed is at https://github.com/Siphon880gh/brain-notes

Before you continue, you need to know the glossary:
- **Attachment folder**: Your Obsidian is usually set to a folder called Attachment where your pasted images create copies into, so that your document can render images while you’re reading a document in Obsidian. However, when you upload the md files online, there is no relative or absolute path to your images (Obsidian had indexed the image path without affecting the MD documents). To setup your attachment folder, refer to [[Obsidian - Setup Local Attachment Folder]]

Below is the rough outline how self hosting published notes is implemented:
- have a npm script that you can run from obsidian terminal which will git push then open a php file on your server. that php file will shell exec a git fetch and hard reset to that origin/main, and in that way merge conflicts didn’t matter. So essentially when you say “deploy” at Obsidian notes, it pushes up to github/gitlab as an intermediate point, and then the remote server pulls from github/gitlab. Therefore, people can see your notes online! 

- your online website will actually render the md files as navigationable and renders as html (for example, markdownit js library). this is done through my devbrain [https://github.com/Siphon880gh/brain-notes](https://github.com/Siphon880gh/brain-notes)

- If you have a lot of md files, it will be less performant when rendering the tree of your content. Then you want when running deploy from obsidian to build a json file of the tree of folders and files and increment the version (to solve sticky cache when it comes to json files)

- the images are a bit trickier. have your markdown renderer look for `![](...ext)`  or `![[...ext]]`  where ext could be png, jpg, jpeg, gif. OR have it looked for `<img>`  after rendering (in which case you look for the lack of https, http, .com, etc in the src), and then prepending a url path to where your attachments would be uploaded to. Your deploy script probably would call a git upload script that checks if the Attachment folder has files that aren’t on your remote SFTP server.  
	
> [!note] Imgur is not recommended for image hosting
> This was easier with imgur since Obsidian, the imgur plugin automatically takes your pasted images and uploads them online to imgur and then replaces the pasted image in the md file with the direct link to the imgur file - HOWEVER, imgur will close accounts randomly especially if they audited you’re only using it for Obsidian or there’s inactivity.
