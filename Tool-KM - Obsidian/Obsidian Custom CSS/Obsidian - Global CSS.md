Manually

```
vi .obsidian/snippets/custom-test.css
```


```
.markdown-preview-view {  
        background-color:red !important;  
        font-size: 200% !important;  
}
```

Warning `body {}`  doesn’t work. It’s `.markdown-preview-view`  that works

If prefer a GUI
Install Obsidian community extension: "CSS Editor" by Zachatoo

CMD+P
![](zMKecQd.png)

----

Settings → Appearance → CSS Snippets will show your custom-test.css and all css files you saved, that you can enable.

![](y7fF9WJ.png)

![](TjQQDw7.png)
