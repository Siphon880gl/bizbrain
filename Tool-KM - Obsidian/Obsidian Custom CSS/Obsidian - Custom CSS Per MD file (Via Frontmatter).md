
CMD+OPT+I

Elements panel. Search for “markdown-preview-view”

![](QFacroi.png)

If you want to style only specific MD files, you can add frontmatter YAML that changes the HTML of that page by adding a custom css (Obsidian is HTML-CSS-JS CodeMirror app):

```
---  
cssClass: customTableStyle  
---
```


![](Zn1GoRF.png)

```
.customTableStyle {  
        background-color:red !important;  
        font-size: 200% !important;  
}
```

---

## Reworded

In Obsidian, you can define custom CSS classes in the YAML front matter of a Markdown file to style specific notes. Here's a quick overview:

### How to Define a Custom CSS Class in a Markdown File

You can add the `cssClass` field in the YAML front matter like this:

```yaml
---
cssClass: customTableStyle
---
```

### Steps to Make It Work:

1. **Enable Custom CSS**: Ensure that Obsidian's "CSS Snippets" feature is enabled in Settings → Appearance → CSS Snippets.
![](6ThtToW.png)


1. **Create a Custom CSS Snippet**:
    
    - Locate your Obsidian vault's `.obsidian/snippets/` directory.
    - Create a new `.css` file (e.g., `customStyles.css`).
    - Add your desired styles targeting the `.customTableStyle` class. For example:
        
        ```css
        .customTableStyle table {
            border-collapse: collapse;
            width: 100%;
            background-color: #f9f9f9;
            border: 1px solid #ddd;
        }
        
        .customTableStyle th, .customTableStyle td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }
        
        .customTableStyle th {
            background-color: #4CAF50;
            color: white;
        }
        ```
        
3. **Activate the Snippet**:
    
    - Go back to Obsidian and reload your CSS snippets by toggling the button in Settings → Appearance → CSS Snippets.
    - Activate your `customStyles.css` snippet.
4. **Apply the Style**:
    
    - In any Markdown file where you want the style to apply, include the YAML front matter with `cssClass: customTableStyle`.
        
    - Write your table in the note like so:
        
        ```markdown
        | Header 1 | Header 2 | Header 3 |
        |----------|----------|----------|
        | Row 1    | Data 1   | Data 2   |
        | Row 2    | Data 3   | Data 4   |
        ```
        
    
    The custom styles defined in your CSS snippet will automatically apply to the note.
    

### Notes:

- The `cssClass` field can support multiple classes separated by spaces, e.g., `cssClass: class1 class2`.
- Make sure the styles in your CSS snippet are scoped correctly to avoid affecting unintended elements in other notes.


