

Wider dataview

.obsidian/snippets/wide-dataview.css:

Settings → Appearance

![](KD7kqGu.png)

![](6uE5Ucw.png)


-->

![](BprWgAE.png)

The snippet you want at .obsidian/snippets should be:

```
.wide-dataview .table-view-table {  
    font-size: 12px; /* Example: Adjust the font size */  
    max-width: unset !important; /* Example: Allow table to extend */  
}  
.wide-dataview .cm-sizer {  
   margin: 0 !important;  
}  
  
.wide-dataview .cm-content {  
  padding: 0 60px !important;  
  width: 100vw !important;  
  max-width: unset !important;  
}
```

--

No, the dataview output is read-only but you can go to the file by clicking the filename then adding the frontmatter fields, in this case:

```
---  
More-Descriptive:  
Specific-Enough:  
Measurable/Metrics:  
Achievable/Feasible/Realistic:  
Relevant:  
Time-Bound:  
EFFORT:  
IMPACT:  
force_rank:  
Summary:  
---
```

So it’s good practice to have a template you can copy and paste, next to the Dataview output

![](rkmMe0G.png)
