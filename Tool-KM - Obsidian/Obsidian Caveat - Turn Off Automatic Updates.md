You should turn off Automatic Update

Each time Obsidian updates, it turns off all your community plugins. It even breaks some community plugins then you have to hope it’s actively developed and that the developer will update it for the new version.

Example of a plugin that gets broken by version updates:
Custom File Explorer Sorting by SebastianMC
However they have kept it updated even so far Fall 2024.