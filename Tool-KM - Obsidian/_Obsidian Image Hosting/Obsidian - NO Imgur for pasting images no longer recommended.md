## State of Affairs

Obsidian offers a community plugin that allows users to easily paste images into their notes, automatically uploading them to Imgur. 

**DO NOT** use imgur. You should stick to local image storage. You can setup the Attachment folder for pasting images by referring to [[Obsidian - Setup Local Attachment Folder]]

Why do not use imgur:
In **January 2025**, I encountered a critical issue—losing access to my Imgur account.  Upon researching online, I found that other users have randomly gotten their Imgur accounts deleted or bugged for no reason. Therefore, I decided to swore off imgur and stick to only local attachments.

## Archived for reading purposes

When pasting pictures into Obsidian md files, they are saved as pasted images in an `_Attachment` type folder. This will quickly eat up your harddrive space. You could have it automatically upload the picture to imgur instead.

---

Install Obsidian extension "obsidian-imgur-plugin"

Make sure to grant permission
![](uPHttrb.png)



---

Create an account at imgur if you don't have one. 

---

In 2024:
Imgur no longer allows you to upload via your login without your own client ID. They were surpassing their limits. Now you have extra steps to setup imgur to work with Obsidian

---

We now need to authenticate Imgur inside Obsidian

1. Go to [https://api.imgur.com/oauth2/addclient](https://api.imgur.com/oauth2/addclient)
2. Use any name for "Application name". For example: "Obsidian Imgur plugin"
3. Choose "OAuth 2 authorization **with a callback URL**"
4. Important: use `obsidian://imgur-oauth` as an "Authorization callback URL" value
5. Fill in the "Email" field and proceed to get your Client ID

---


Now go back to your Obsidian `Settings` > `Imgur Plugin Setting`
- Switch `Images upload approach` to `Anonymous Imgur Upload`
- A new field `Client ID` will appear, fill it with the `Client ID` generated after completing step 5
- You can find and manage your `Client ID`s [here](https://imgur.com/account/settings/apps)
- You don't need the secret key for this to work.