At the Ribbon:
![](SBqHcqa.png)

This creates a canvas that lets you add visual elements on a canvas. Note bottom buttons:
![](VYw10hy.png)

- Drag to add card
- Drag to add note from vault
- Drag to add media from vault

Cards let you add text inside and connect cards to each other to show relationships.
![](L6cV112.png)

In order to connect cards, move your mouse over a side of the card to reveal a circular handle. Click and drag it to connect one card to another card:


| ![kO3gsAJ.png](kO3gsAJ.png) | ![](HV8WUYr.png)<br> |
| ----------------------------------------------- | ---------------------------------------- |


Alignment and snapping is supported. You can paste images (here I copy/pasted a cropped screenshot with CMD+SHIFT+4) and have it synced to imgur if that's been setup.
![](veTLpJS.png)

Text formatting options are non-existent as of 1/2025.
![](wzTrFB3.png)

If curious, you can open the canvas file in VS Code or any text editor and you'll see it's a json file:
```
{
	"nodes":[
		{"id":"da878128a4206fb1","type":"text","text":"![](q8Jxb1i.png)","x":-125,"y":-121,"width":250,"height":50},
		{"id":"5aad19f6ae93f225","type":"text","text":"This is some text. Text formatting options are non-existent as of 1/2025","x":-125,"y":-220,"width":250,"height":90}
	],
	"edges":[]
}
```
