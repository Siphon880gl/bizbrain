
```
$\times$
```

But you don't type into a code snippet. Above example becomes x

More:
https://www.reddit.com/r/ObsidianMD/comments/onw4ak/how_to_write_mathematical_formulas_in_obsidian/


And you can do fractions
```
$\frac{100}{20}$
```
![](VuRs97F.png)


----


(13842  x 12) / (1-.3)
->

```
$\frac{13842 \times 12}{1 - 0.3}$
```

$\frac{13842 \times 12}{1 - 0.3}$
