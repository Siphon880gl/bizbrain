
```
T(n<sub>1</sub>) + T(n<sub>2</sub>) 
```

Remember CMD+E to switch between code and render. When in code mode, you may have to click the subscript to see its markdown code

T(n<sup>1</sup>) 