Obsidian can:

$x^3+x^2+x+1$
$E = mc^2$


Devbrain can:
`@(1/2[1-(1/2)^n])/(1-(1/2))=s_n@`
`$E = mc^2$`

---

- **KaTeX**:
    
    - KaTeX typically uses dollar signs (`$`) to delimit inline math formulas.
    - For example:
        - Inline math: `$a^2 + b^2 = c^2$`
        - Displayed math (centered on its own line) uses double dollar signs: `$$a^2 + b^2 = c^2$$`.
- **AsciiMath**:
    
    - AsciiMath uses `@` symbols to delimit math expressions.
    - For example:
        - Inline math: `@a^2 + b^2 = c^2@`
        - Displayed math would still use the same `@` delimiters but could be written on its own line.

---

Test snippet:

```
`@(1/2[1-(1/2)^n])/(1-(1/2))=s_n@`
`$E = mc^2$`
```