---
topic: research
---
## Show all files at a directory

```
\```dataview
TABLE theme
FROM "_Obsidian Todos (For more mastery)/Unsorted Todo"
\```
```


---


## Show files with certain property key values

![](Frc1qAc.png)


```
\```dataview
LIST
WHERE topic = "research"
\```
```

