
Obsidian permits tags both in the YAML (e.g. `tags: todo`) and in the body (e.g. `#todo`). Both methods work fine, and which you use is up to your personal preference. Dataview can see tags in either place too.

Remember that means 
```
---
Tags: one two three
---
```

Or:
#one #two #three

~Broaden: For querying tags and more:
https://blacksmithgu.github.io/obsidian-dataview/queries/structure/


Then, you can:

#source/book/c

Screenshot:
![](mkENjxV.png)
