
Here, all notes with “test” in the **title or body** are highlighted red
```
test
```

For only notes with the word in the **title:**
```
file:PRIMER
```