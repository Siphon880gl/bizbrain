
Search inside a folder:
Right click folder in navigator → Search in folder
![](lizjEcf.png)

Then that will open Search panel with the query typed in to limit the path downwards:
![](nWK57Uh.png)
