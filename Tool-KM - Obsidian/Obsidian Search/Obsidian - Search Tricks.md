
Yes you can search exact phrase with quotes:
```
"Prioritize tasks"
```


----

Your search results are too much?

The search formula is:
```
Framework path:"Biz Toolkits"
```

When you’re searching a term, you can then type path: then type wildcard, and it’ll autocomplete. You select the path that is likely (best an ancestor path that contains the possible folders of content with your search subject)

Eg. Say Im searching for the word "Framework". Here are the subsequent steps
Framework
Framework path:
Framework path:Biz
Framework path:"Biz Toolkits"

