Lets say your MD file is rendered by js libraries like markdownit (or you might in the future). 

If have code block ender inside the collapsible section, make sure it isn’t the final line of the collapsible. if needed, add a blank line that also starts with `>` . Otherwise it’ll render everything else inside the collapsible.

See caveat ignored:
![](36oeqeo.png)

And that collapsed the rest of the content “When using any of their scraper preset...” which is intended to be outside and below the toggleable.
![](VzetW5l.png)

---

But by implementing the solution, you see a clear separation between collapsible and the rest of the content:
A:
![](KY37UPF.png)

B:
![](Ue2kQi8.png)
