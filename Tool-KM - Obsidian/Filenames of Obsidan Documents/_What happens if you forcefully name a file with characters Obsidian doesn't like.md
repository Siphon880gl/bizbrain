If you name the note in the note editor or rename the file at the sidebar, it will simply not let you

But if you named it in your Explorer or Finder, Obsidian can't stop you. Clicking that file in Obsidian will try to open the file with its default app for the file extension.

In a similar way, json files which OBSIDIAN does not read as of 1/2025, will open with its default app for .json (Eg. VS Code)