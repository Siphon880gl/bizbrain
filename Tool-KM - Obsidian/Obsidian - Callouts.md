Callouts help isolate some information into a neat colored box.

```
> [!info] 
> Here's a callout block.
> It supports **Markdown**, [[Internal link|Wikilinks]], and [[Embed files|embeds]]! 
> > ![[Engelbart.jpg]]
```

Renders as:
![](AeWp23Y.png)

If you can't get it to render, look for the community plugin Admonition

All callouts available:
https://help.obsidian.md/Editing+and+formatting/Callouts

---

There are different types of callouts and each with a different color. Type `>[!`note] and wait for an autocomplete dropdown to appear:

![](edGidWH.png)

---

Pasting multiple lines and it breaks apart the callout?

Paste with CMD+SHIFT+V