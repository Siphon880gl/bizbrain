Eg. Searched and search results
```
test
```

```
2x2 matrix
Test your app
Jest Tests
```

Versus, Eg. Searched and search results
```
test -file:2x2
```

```
Test your app
Jest Tests
```

---

One straightforward way to exclude (hide) a file from Obsidian’s **Graph View** is to use the **Filters** feature. Essentially, you instruct Obsidian’s graph to ignore (filter out) that specific file (or multiple files).

Here are a couple of approaches:

---

## 1. Use the Global Graph’s Filter Field

1. **Open your Graph View** by clicking on the **Graph** icon in the left toolbar or using your preferred hotkey.
2. In the Graph View’s right sidebar (the settings panel), find the **Filters** section.
3. In the **Query** field, add a negative (`-`) operator on `file:` or `path:` referencing the note’s name or path. For example:
    
    ```
    -file:"YourNoteTitle"
    ```
    
    or if you know the exact path or partial path, you can do:
    
    ```
    -path:"SomeFolder/YourNoteTitle.md"
    ```
    
4. Press **Enter** or click outside the filter box; the file and any edges to/from it should disappear.

**Note**: If your note name has spaces, wrap it in quotes, e.g., `-file:"My Note With Spaces.md"`.

---

## 2. Use the Local Graph Filter (for a single note’s neighbors)

If you only want to hide certain backlinked files in a **Local Graph** (the graph that shows a single note’s connections):

1. **Open the note** you want to explore.
2. Use the command palette (Ctrl/Cmd + P) and search for "**Open local graph**."
3. In the local graph’s **Filters** panel (also usually on the right side), add a negative query to exclude other notes. For example:
    
    ```
    -file:"NoteToHide.md"
    ```
    
    This will prevent connections to `NoteToHide.md` from showing in the local graph.

---

## 3. Put the File in an Ignored Folder (Advanced)

Another approach is putting files you never want in the graph (for example, work-in-progress notes or templates) into a folder that is **excluded** from the vault’s indexing:

1. **Go to Settings → Files & Links** (or sometimes Settings → Excluded Files).
2. Add that folder’s path to be ignored if Obsidian supports ignoring it from indexing (this depends on your Obsidian version; in some older versions, there was no direct “ignore” folder option, but you can do it with `.gitignore` in the vault or advanced settings).

This method hides the entire folder from the vault’s knowledge, so _none_ of those files will show in the graph, in autocomplete, etc.
