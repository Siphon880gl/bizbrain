

Graph View (Left ribbon -> Graph View):
![](lWAoYS1.png)

Local Graph View (CMD+P -> Open local graph):
![](HZihQ1l.png)

Graph View has Orphans option. Local Graph View does not.

Orphans are notes that haven't been linked and are just floating around in the graph view. Turning on Orphans will let you see which thoughts/notes still need to be connected, useful in Zettelkasten or writing a thesis.
