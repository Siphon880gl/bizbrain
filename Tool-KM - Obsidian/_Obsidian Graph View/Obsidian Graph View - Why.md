
Obsidian's graph view is a powerful visualization tool that helps users discover and explore connections among their notes. It represents notes as nodes and links between them as lines, offering both global and local graph views[1][3].

You open at the left strip ribbon menu:
![](2DWylvw.png)


## Key Features

**Node Representation:** Each note is displayed as a circle (node) with its title. The size of the node increases based on the number of references it receives[1].

**Link Visualization:** Lines between nodes represent internal links between notes[3].

**Filtering:** Users can customize the graph view using search terms, tags, and groups to focus on specific subsets of notes[1].

**Local Graph:** This feature displays connections between the active note and related notes, with adjustable depth levels[3].

## Benefits for Finding Connections

1. **Serendipitous Discoveries:** The graph view can reveal unexpected relationships between notes, helping users identify patterns they might have overlooked[1][2].

2. **Visual Exploration:** Users can hover over nodes to highlight connections, click to open notes, and navigate through their knowledge base visually[3].

3. **Customizable Views:** By using filters and groups, users can create targeted visualizations that surface the most relevant connections for their current needs[1].

4. **Time-lapse Animation:** This feature allows users to see how their notes and connections evolve over time[3].

5. **Local Graph Insights:** The local graph view helps users focus on the immediate context of a specific note, revealing its connections at various depths[3][7].
   
6. **Orphaned Notes**: Identify orphaned notes, then you can link them to your greater world of notes. Refer to [[Obsidian Graph View - Find Orphaned Notes]]

By leveraging these features, Obsidian's graph view becomes a powerful tool for discovering and exploring connections among notes, enhancing the user's ability to generate insights and navigate their knowledge base effectively.

Citations:
[1] https://mindmappingsoftwareblog.com/obsidian-graph-view/
[2] https://forum.obsidian.md/t/graph-connections-between-links-within-a-note/71451
[3] https://help.obsidian.md/Plugins/Graph+view
[4] https://forum.obsidian.md/t/design-talk-about-the-graph-view/22594
[5] https://www.reddit.com/r/ObsidianMD/comments/13xpnfx/is_there_a_plugin_that_allows_graph_view_to/
[6] https://www.youtube.com/watch?v=Rsdxl7F9kBY
[7] https://www.youtube.com/watch?v=5x5ua7LecOI
[8] https://thesweetsetup.com/the-power-of-obsidians-local-graph/
[9] https://forum.obsidian.md/t/how-to-see-connections-other-than-using-the-local-graph-and-backlinks-panes/26099
[10] https://forum.obsidian.md/t/connect-notes-in-graph-view/12747