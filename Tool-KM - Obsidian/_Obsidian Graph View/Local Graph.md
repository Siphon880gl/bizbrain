Obsidian graph view has normal graph and local graph. Local graph will be the graph view from your active note. This is a closer look if you have too many things going on in Graph that even filtering doesn't help it.

Do CMD+P for command palette and search for "Open local graph".

There's "Neighbor links" and there's without. "Neighbor links" OFF will only show our active note linking to other notes. "Neighbor links" ON will show your active note linking to other notes and those other notes linking to other notes

Rephrased from: https://forum.obsidian.md/t/local-graph-what-are-neighbor-links/6954/2