In Graph View, you can highlight notes in a specific color based on query (Like you would in search panel)

Here, all notes with “test” in the TITLE OR BODY are highlighted red
```
test
```

![](1bkVHUF.png)
For only notes with the word IN THE TITLE:
```
file:PRIMER
```


---

The coloring is based on query entered at Groups section:
![](TTb9T6w.png)
