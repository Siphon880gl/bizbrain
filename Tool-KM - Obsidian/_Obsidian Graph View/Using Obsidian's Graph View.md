Open graph view
- Click on the left side bar ![](4VxhSLC.png)

![](prDWNQd.png)


Do filters. You can create groups to color code the nodes

![](jBlxOFE.png)


This shows you all accounts / services not mapped. I had a MoC that link to them using `[[...]]` notation.
![](paUNFhK.png)
