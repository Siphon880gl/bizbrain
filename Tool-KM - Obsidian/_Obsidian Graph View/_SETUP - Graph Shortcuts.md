Recommend you keep the default shortcut
Open Graph View - CMD+G

Recommend you add the shortcut
Open Local Graph - CMD+L
It will complain of conflicts with Toggle checkbox. You should sacrifice Toggle checkbox.

With Local Graph, you can see in and out references from the current note.
Reminder: Setup shortcuts at Obsidian -> Settings -> Hotkeys