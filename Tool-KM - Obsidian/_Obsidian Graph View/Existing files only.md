
Turning on "Existing files only" will only show those files that exist, does not show those that are linked and not yet created. Here is an example of a document that's not been created yet:
[[Not created yet]]
And therefore, leaving "Existing files only" OFF will display:

![](xU3sKgw.png)
