
Install extension: Kanban by mgmeyers

To create a new Kanban:
1. CMD+P Command Palette for Kanban:
   ![](NAl2IvM.png)


There's board view, table view, and list view. Top right to switch:
![](yWAZSyt.png)

To switch between those and MD view: 
- CMD+J. 
- Proof:
  ![](svVWzae.png)

- MD View is just headings with checklists, but notice Frontmatter YAML property "kanban-plugin":VALUE where value is board, table, or list. So when you toggle back from MD file, it knows which to open (Kanban Board vs Table vs List)
  ![](F2Vinmq.png)
  

To edit card in board view, click inside card and edit. Press Enter to commit. Press shift+Enter for next line in the card (for if you're still editing)

To delete a card in a list/column, click the "..." menu for the card then go to "Delete card".
![](32RqCbl.png)


---

Glitched? If pressing enter to commit doesn't save the card, then switch to table or list view, then switch back to board view. Was a bug.