## How to create, manage, and use Kanban Boards in Obsidian

Install and activate **Kanban** at community plugins.

CMD+P for command palette to type Kanban and look for the Create Kanban command. It'll be created as an "Untitled Kanban" at the root. You can toggle back to MD at the top-right "..."

To toggle back from MD to Kanban, you have to CMD+P for command palette to type Kanban and find the toggle command.

---

Added these shortcuts (K shortcut is taken and universally means new link between Notion and Obsidian)

![](Zk1Msis.png)


---


CMD+P to create new Kanban or toggle between Kanban/Markdown

When in markdown you can see archived

When in markdown view you can move tasks between archived and a list by cut and pasting a checkbox item (Each checkbox item is a card)

---


While in Kanban mode:

You create a list, naming it. Then to create a card inside a list,

```
**Kanban Title**
Hello world

```

So you type the double asterisks for the title, then you can CMD+Right to the end of the line, then press Shift+Enter to type the rest of the body. Press Enter to commit the card in the list.

You can drag cards between lists and rearrange the cards' order in a list.