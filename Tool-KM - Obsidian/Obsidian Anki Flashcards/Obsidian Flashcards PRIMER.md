Having a MD file like this:
```

inline front::inline back


inline back v2:::inline front v2


## front1 #card 
Yep this is the back
this is an additional line
^this is another line and notice it still shows with 


## front2 #card/reverse 
has been reversed


Without heading #card 
Still shown without heading hashtag


## spaced3 #card/spaced


<!-- clozeblock-start -->
The capital of Japan is ==Tokyo==
<!-- clozeblock-end -->
```

You can generate a deck of flash cards in Anki app

---

Requirements:
- Installed Anki https://apps.ankiweb.net/
- Installed AnkiConnect Add-on which is required to connect Anki to any other app:
	- Inside Anki, you enter its add-on code in order to install AnkiConnect
	- You can find the code at AnkiConnect's official webpage https://ankiweb.net/shared/info/2055492159
  ![](GFcINmF.png)

![](9ckSvXC.png)


![](n4Cb5RR.png)

- Installed Obsidian Extension: Flash Cards by Alex Colucci

---

**Setup Obsidian to Anki:**

At Flash Cards extension, click "Grant Permission" (otherwise clicking Test will simply says it cannot connect)
![](Sf0hX47.png)

![](rcdpj8w.png)

---

At your MD file that has the proper syntax for flash cards, you can generate the deck in either way:

**Approach A**: CMD+P command palette to generate flash cards
![](IV0bF11.png)


**Approach B**: Click Generate flashcards icon at side ribbon
![](W1STJJk.png)

---

**How it looks**

Refer to example at the top of this document, which should be obvious how the syntax may work for different styles of flash cards.

For this syntax:
```
## front1 #card 
Yep this is the back
this is an additional line
^this is another line and notice it still shows with 
```

Its Anki card that's been finished (flipped to back already, so both front and back are shown):
![](2TBRWEg.png)

---

**Caveat on future migrations**

Note if it generated card ID's (In this case 1671769213...), in the future if you migrated the notes to another computer, you'll have to remove these ID's, otherwise generating flash cards will error that those Anki ID's don't exist. 

You can perform a Regex search and replace with CMD+P for command palette then going for "Regex Find/Replace"

The extension author might decide to remove Anki ID's in the future.
![](QGcFS4I.png)
