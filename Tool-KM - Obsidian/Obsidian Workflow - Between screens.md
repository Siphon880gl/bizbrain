
## Shortcuts

Obsidian expand to and highlight file into view
CMD+SHIFT+R

Obsidian to Terminal:
CMD+ALT+R

Mac Finder to Terminal;
CMD+SHIFT+X
That's with a folder selected. Doesn't work to open terminal to the folder you're opened in. In that case, you can go up one folder with that current folder selected firstly, so:
CMD+Up -> CMD+SHIFT+X

Terminal to Obsidian
??

---

## Setup Obsidian
Obsidian -> Preferences -> Hotkeys
![](G8AxBH2.png)

## Setup Mac Finder to Terminal
![](EED9jbf.png)

