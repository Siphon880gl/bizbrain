
Problem: Code Snippet ruining bullet format continuity below the code snippet

Demonstration: The text following the code snippet no longer follows the bullet format
![](knITBSq.png)


---

Solution:  
Select code snippet including the \`\`\` at the start and end:
![](vqbKwJ9.png)

Press tab until lines up. Now is fixed:
![](VO1OzC0.png)


Notice that “MongoDB 3.4 and below...” and “Above Mongo 3.4...” and “While in Mongoshell” lines are all indented too without bulletpoints (Shift+ENTER) to create a new line at the same indentation with the content. Without this, the solution fails as well because you can’t have out of indentation tildes or other lines