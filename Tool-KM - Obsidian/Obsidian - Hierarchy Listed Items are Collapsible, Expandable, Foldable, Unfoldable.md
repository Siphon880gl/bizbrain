Some lines are collapsible

1. Hi
	1. And this can be collapsed
	2. Move mouse over to the left of first 1. for a down caret


Requirement:
Must had fold enable in Obsidian.
Editor -> Fold Indent (This one makes bullet points fold work)
Editor -> Fold Headings (this one makes heading fold work)
