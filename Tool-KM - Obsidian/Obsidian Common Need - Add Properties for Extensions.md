Adding properties to MD files lets your extensions like Dataview and Breadcrumb to work based on "settings" or "properties" of your notes.

When reading about file properties online, you may see them referred as Frontmatter or YAML.

Add the properties interface by adding to the first line three dash -'s:
```
---
```


^ There is more than one way to do so, actually:
- Use the **Add file property** command.
- Use the **`Cmd/Ctrl+;`** hotkey.
- Choose **Add file property** from the **More actions** menu (three dots icon) or right-clicking the tab.
- Type `---` at the very beginning of a file.

Then it'll render the properties UI:
![](xJioK9K.png)

---

To remove specific properties:

Right click the property's icon -> Remove
![](JK5TvLm.png)

---


To remove ALL properties and the properties UI:

Right-click Properties section title -> Clear file properties

![](WTBAiAz.png)
