Setup:
Rearrange icons of plugins on the left side thin bar. And have it persist to next time you reload Obsidian


In Obsidian: CMD+Opt+I

Inspect mode
![](A5SuzDu.png)


Get aria-label

Then copy this code:
```
div.side-dock-actions {
    display: flex;
    flex-direction: column;
}

div.side-dock-actions div[aria-label="Appropriate text value"] {
    order: -1; /* order earlier */
}

div.side-dock-actions div[aria-label="Appropriate text value"] {
    order: 1; /* order later */
}
```

Place into your <obsidianVault>/.Obsidian/themes/Things/theme.css

From:
https://forum.obsidian.md/t/customisation-of-order-of-icons-in-left-toolbar-panel/7193/13#post_14
