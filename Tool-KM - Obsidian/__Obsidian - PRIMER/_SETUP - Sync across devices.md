
Use Obsidian's services

Otherwise, use Google Drive to sync the files on PC to the Cloud

For mobile, use GoCoEdit to sync Google Drive to Mobile. GoCoEdit can support opening MD files. 
- If you are using the phone for reading (no editing), but use the computer for editing, then you can turn off On-Screen Keyboard by default in GoCoEdit.
- Note GoCoEdit can bug on some phones and have problems reloading the Google Drive files. If you use Obsidian/GoCoEdit for critical account login credentials, you can open a Google Drive app (MD files don't open in Google Drive, but it can be opened by your phone after downloading from Google Drive to phone Files)
	- iPhone Files can support MD files
	- Android Files need another app to read MD files