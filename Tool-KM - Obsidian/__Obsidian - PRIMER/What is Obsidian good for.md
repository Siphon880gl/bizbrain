KM - Knowledge Management because you can have folder hierarchy. You can tag. You can use extensions to improve the knowledge management.

You own your files because if Obsidian as a company shuts down, you still have the files on your computer. It's recommended you backup or sync to Google Drive or OneNote, or use Obsidian's syncing services.

Zettelkasten friendly? 
- Yes, AND can easily find Orphaned thoughts with Obsidian's Graph View.