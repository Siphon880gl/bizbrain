
```
---
sorting-spec: |
  AI App Development
  Game Development, Unreal
  Web Development
  Web Development - Rapid Development
---
```


https://github.com/SebastianMC/obsidian-custom-sort

Click the ribbon button ([![Inactive](https://raw.githubusercontent.com/SebastianMC/obsidian-custom-sort/master/docs/icons/icon-inactive.png)](https://raw.githubusercontent.com/SebastianMC/obsidian-custom-sort/master/docs/icons/icon-inactive.png) or [![Static icon](https://raw.githubusercontent.com/SebastianMC/obsidian-custom-sort/master/docs/icons/icon-mobile-initial.png)](https://raw.githubusercontent.com/SebastianMC/obsidian-custom-sort/master/docs/icons/icon-mobile-initial.png) on phone) to tell the plugin to read the sorting specification and apply it. The sorting should be applied to the folder. On desktops and tablets the ribbon icon should turn ([![Active](https://raw.githubusercontent.com/SebastianMC/obsidian-custom-sort/master/docs/icons/icon-active.png)](https://raw.githubusercontent.com/SebastianMC/obsidian-custom-sort/master/docs/icons/icon-active.png))

---

To pin certain files to the top and certain files to the bottom:
```
AI App Development
Game Development, Unreal
Web Development
Web Development - Rapid Development
%
README
server-update.php
sortspec
package.json
```


----

You can add dividers for human readability in the sortspec but wont affect sorting:

```
Unreal Engine
Blender
-------------------------------------------
AI Gen - 3d model, Animation, etc
Free - 3d Models, Animations, etc Prerequisite Knowledge
```