
Toggle render mode shortcut
CMD+E

---

You should setup the note pinning at [[_SETUP Recommended - Hotkeys, Obsidian]]

This is because often when switching between notes, your previous tab "pops off" and closes from the tabs. You may want to pin a note before opening another note.

---

Your note may benefit from a collapsible section so that the user can choose to continue reading at the same level instead of risking interrupting the flow by delving into details, or so that you can give the user options to read what section by opening the section. 

Refer to [[Obsidian - Toggleable collapsible sections]]