


Hi this is a word that's ~~striked-out~~

**BOLD**

*ITALICS*


---

```
Code snippets
# BIG HEADING
## SMALLER
### SMALLER
```

>Blockquote
>Next line
>Next next line


>[!note] Callout
>a
>b

---

```toc
```

---


# BIG TITLE

## Smaller title

### Even smaller

```
1.
```

1. an item
2. another

```
- a
- b
```

Another type of list
- a
	- b
	- c
- d

Tab and shift tabs to add indentation in a list