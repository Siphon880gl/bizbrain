Aka: Get Started

Obsidian for: Ideation, development research, studying. Is a personal knowledge tool. Syncing and collab requires paid subscription

Follow these tips and objectives to quickly learn Obsidan.

---

For ease just use one vault

How create folders and files

Know how to add a divider then tag other notes after. This your footer. 

So whenever you have a new idea or note you know how it relates to another note then you can tag

How to see the connections simultaneously between notes that are tagged. 

How to see only the note connections clicking the graph view so you have a large screen of all the connections

How add different headings from right clicking. Then how to do short hand with hash characters. And when you inserted earlier you saw visual indicators of the hashtags

How to open table of contents on the right simultaneously

---

Btw in case you dual use for studying :
Plugin flash cards to review notes and mark notes you need more exposure too
