
Recommend you set

Toggle pin: CMD+OPT+P

---

Open integrated terminal at current directory:
![](9Kef3hU.png)
aka control plus tilde

You set it up at Obsidian Settings -> Hot Keys. You probably want to set the shortcut key to opening current directory instead of root directory, and in that way, whatever Obsidian document is opened, then that's the folder the terminal opens in.

If refuses to let you set up that tilde shortcut, it's possibly conflicting with Community Plugin Text Format by Benature