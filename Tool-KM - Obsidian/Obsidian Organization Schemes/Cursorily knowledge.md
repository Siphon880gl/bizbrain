Obsidian is an excellent tool for knowledge management or serving as a single source of truth for individuals. 

However, sometimes the documents you create only capture a cursory or shallow understanding. For instance, you might have merely skimmed a video or glanced at a code snippet, without fully digging into all the nuances and potential pitfalls. Or your note may not be clearly structured because you haven’t yet developed a deep understanding of the subject.

Even so, these preliminary notes can still be helpful—either to delegate tasks to professionals who can fill in the details from their own expertise, or to serve as a reference for you when you have more time to explore the topic in depth. 

To indicate that a note is still at this basic stage, simply add **"(Cursorily)"** to the filename.