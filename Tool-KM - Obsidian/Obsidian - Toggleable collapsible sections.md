
TLDR - Best Obsidian way:

```
> [!note] This is a  callout. 
>  It's collapsed by default unless the reader clicks it to open it.
```

If you messed up the format, it could become a blockquote like:

![](aKpOkid.png)

---


1. Example
Click me

2. Code/Markdown
```
<details>
  <summary>Click me</summary>
  ### Heading

  1. Foo
  2. Bar
     * Baz
     * Qux
</details>
```

----


** For compatible with DevBrain **

Use this:

```
<details>
<summary>Details</summary>

[chrome://extensions](chrome://extensions)  

Or: Chrome → ... -→ More Tools → Extensions. Make sure "Developer mode" is on at the top right  

![](P89AAGz.png)

</details>
```


---

Collapsible callouts

> [!note] This is a  callout. 
>  It's collapsed by default unless the reader clicks it to open it.

Side consequence: If using AI and you want their content inside a callout, you'll have to ask to format:
```
Please rewrite with each line starting with ">" character. Put in a code snippet. Here's the text that needs formatting:




```


![](0wnRs3Z.png)

So the format is `>[!__] ` which emphasizes that it's collapsible with a `>` and that it's a special element [!collapsibleCalloutType] (like inserting an image with `![altText]` but the `!` is inside the square brackets). 

The space then text immediate after lets you set the header line but a header line is optional. 

Press enter to continue typing the body of the collapsible. Click outside to resume your other content.

All types supported:
https://help.obsidian.md/Editing+and+formatting/Callouts

Previewed:

![](7Tc8GaA.png)

![](l2xbnrk.png)
