You can find more competitors by querying for similar companies to you. But if you are a new entrant to the market, that information may not be available to even query. Instead, if you already know your primary competitor, you can query similar companies to the primary competitor.

Let's say you're a pickleball branded product company Selkirk that is well known already.
1. Find similar companies to Selkirk:
	- By Google search: type vs to see dropdown suggestions:
	  ![](LXqoVl9.png)

	- Use similiarweb.com to look up its alternative websites. Unfortunately, SimilarWeb is not free, but they do offer a free trial.