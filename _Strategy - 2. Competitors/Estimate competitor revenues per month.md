
Watch:
https://www.facebook.com/reel/1109785760630481?fs=e&mibextid=0NULKw&fs=e&s=TIeQ9V

And/or refer to this tutorial

---

Sign up for similar web and plug in the URL to see their monthly traffic numbers giving you a sense of their overall monthly reach

You can also dive deeper to see which keywords are driving their traffic which means you’re not guessing which search terms are helping them rank as well as their top performing ads.

Next you want to go to their website to look at what their average order value (AOV) looks like. You don’t have to be accurate. Just take an educated guess based on their products.
Eg. $100 if they’re selling electronics

Google their industry average conversion rate. Eg. Google Search: "walmart industry average conversion rate"
Eg. For e-commerce, it’s usually 2.5% to 3%

If Google isn’t giving good results.

Use the free Perplexity AI for asking industry conversion rate. If it’s not confident, then say it can give a reasonable range.

Multiply their AOV to their Monthly traffic and to the conversion rate

| Monthly Traffic | AOV  | Conversion Rate |
| --------------- | ---- | --------------- |
| 50,000<br>      | $100 | 2%              |

That is $100,000 a month

You’re not just estimating revenue, you’re getting the full picture of their strategy.

---

## Search Industry Conversion Rate
![](MgpVx1U.png)

## Traffic:
![](M5PBq82.png)

![](cWGm8Vj.png)


![](s55AV1g.png)

## Top search:

![](Jhur2EE.png)

## Top ads:
![](teDSfCL.png)

---

Other keywords:
aov