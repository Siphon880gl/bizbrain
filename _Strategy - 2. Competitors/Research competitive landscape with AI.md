Researching competitors

Use Perplexity
https://www.perplexity.ai/
^ Why: Better for research

---

Prompts (Use same chat)

PROMPT 1: Find similar companies to bookwitheva.com FOR big events. I'll ask about smaller venues later
^ Why: Provides context for the rest of the chat

PROMPT 2: Now give me a competitive landscape of 4 quadrants or more of niches and features across that industry. Big venues, mid, and small included.
^ Why: It understands the 4 quadrant. But you give it permission to give a deeper analysis (otherwise would be too shallow)

PROMPT 3: Give me a mermaid diagram of your analysis. Add more if there are

Take that code and input it into Mermaid Live Editor:
https://mermaid.live/

You get back:
![](ZCYzZOk.png)
