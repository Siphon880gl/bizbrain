Click options for your section header, and click "Copy link to block":
![](4tWA7SJ.png)

Select the text
![](PuIVoMW.png)

Then CMD+V to paste over it. The text will link to that section, clicking it would jump to that section.