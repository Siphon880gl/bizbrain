OPT+Click open in side bar
But sucks if you have to move/duplicate items from side bar to page because the sidebar could cover the page, and to work around that, your left side of page should have a dropping area, maybe named Dropping area from Sidebar

OPT+Drag to duplicate to new destination

CMD+[ (Chrome. shortcut) go back to previous page in panel or previous page on Notion

SHIFT+Opt+Click to open new window, then perform split screen (If Mac not support, get BetterSnap Tool and configure as CMD+Left and CMD+Right)
You cannot drag items to move or duplicate between two windows

OPT+Click will fail to open a Gallery item in sidebar, even though clicking options will show sidebar as an option with the same shortcut key in the menu option. As long as Notion doesn’t fix this glitch, the quickest way to open a Gallery item in sidebar is:
- Click ... on the Gallery Item
- Search options is already in focus so start typing: “si”
- And press Enter (no need to type fully sidebar because sidebar will auto suggest)

Accidentally closed the sidebar? It would break your workflow to scroll back to where you had clicked the link to open sidebar? Just go CMD+[ because navigating back restores the DOM state of Notion.