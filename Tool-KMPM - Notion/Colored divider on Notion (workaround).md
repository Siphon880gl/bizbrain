
Leverage Latex rendering in Notion to create colored dividers

Looks like:
$$\color{red}\rule{5000px}{3px}$$  
$$\color{#bf847e}\rule{5000px}{3px}$$ 

In a code block for your convenience. Note the color which can be plain English or hex, the width of the divider, and the thickness of the divider:


```
$$\color{red}\rule{5000px}{3px}$$  
$$\color{#bf847e}\rule{5000px}{3px}$$
```

Hex values can be found here:
[https://www.color-hex.com/](https://www.color-hex.com/)  

Copying into Notion:
- If you copy and paste above into Notion, and if it turns into red text in gray background, it’s unfortunately been converted into Code block. In that case, paste unstyled instead (Mac is CMD+Shift+V). If all else fails, open Notion’s equation dialogue to input into (CMD+SHIFT+E ).

Limitations:
- The resulting colored divider is not confined to the block, meaning it doesn't stay within a column's boundaries.
- It is not responsive. For example, if you set a significant length (e.g., 750px), it won't adjust its size when viewed on mobile devices.