
Run in DevTools console.

Untick all ticked checkboxes (great for checklists)

only for desktop unless you can access the browser console on mobile


```
document.querySelectorAll('*').forEach(checkbox => {  
    if(checkbox.matches("input[checked]")) {  
        console.log(checkbox)  
        //checkbox.checked = false;  
        //checkbox.removeAttribute("checked");  
        checkbox.click()  
    }  
});
```

The usual javascript of changing checked attribute or removing checked attribute does not work because of how Notion is coded to handle checkbox state. You have to trigger a click