```
formatDate(now(), "DD/MM/YYYY") # 10/01/2025  
formatDate(dateAdd(now(), 1, "days"), "DD/MM/YYYY") # 10/02/2025  
formatDate(dateAdd(now(), 2, "days"), "DD/MM/YYYY") # 10/03/2025  
```