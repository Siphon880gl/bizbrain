
### Equation
CMD+Shift+E lets you type in latex equation, then it renders the math symbols. In fact, if your latex code is incorrect, Notion renders a "Invalid Equation"

### Formula
This is a property type that lets you type Excel-like formula's