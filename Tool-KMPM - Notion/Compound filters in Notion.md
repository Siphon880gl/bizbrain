Free version can have OR/AND between filters in column

For example this is a subscription table where I track payments on the 4th/5th/6th/7th at a view, which I named ~6th (approximately 6th)

![](SS0c7hk.png)


And the Views can be:
![](Ie6alcD.png)
