When you name a block and you create an inline database/board/etc, but that database has a name too, then there could be some visual collision or redundancy.

Notice "Week of" is shown twice:
![](krAZrSP.png)


---

You desire this instead:
![](uqBeVcx.png)

---

You can go into the options of the board, in this case. Then turn off its title:
![](7kGAPaM.png)
