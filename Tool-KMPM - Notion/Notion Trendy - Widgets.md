
You can embed Widgets

![](ueXyo1U.png)

Service:
[https://indify.co/](https://indify.co/)  

---


Another widget service:
https://apption.co/

More widgets: Filter Description → Widget
https://kcg001.notion.site/Ideas-and-Resources-18ba6ed47aac4a37a2005251f810d910

- Kairo https://getkairo.com/notion-blocks
	- Cost: Free or $3.75/mo (14-day trial) for more features.
- Notion Sparkles https://notionsparkles.com/sparkle-forms
	- Cost: Free to use.
- WidgetBox https://www.widgetbox.app/
	- Free plan lets you add buttons into Notion, lmao
	- Cost: 5 widget slots in the free plan. To create unlimited widgets, upgrade to pro (0.99$/month).
- Weather Widget https://weatherwidget.io/
	- Cost: Free to use.
- Notion VIP https://www.notion.vip/charts/
	- Cost: Free to use.

Even more
- Notion Hero
- EmbedNPages

More:
https://upqode.com/alternatives-to-indify-notion-widgets/

---

Shopify playlist can be embedded

![](N11x80V.png)
