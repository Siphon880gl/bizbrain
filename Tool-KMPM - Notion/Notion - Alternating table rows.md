

```
document.body.append(
(()=>{
    var styleEl = document.createElement("style");
    styleEl.innerHTML = `
    .notion-collection-item:nth-child(odd) {
        background-color: rgba(0,0,0, 0.1) !important;
    }
`
    return styleEl;
    
})()
)
```