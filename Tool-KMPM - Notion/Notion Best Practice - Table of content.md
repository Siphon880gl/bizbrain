
Firstly, how to insert toc, you type `/` then:
![](OQO9TDF.png)

Your `#` and `##`, etc, you should name them like so:
```
# I Need Help with...
## ... Finding my life purpose
## ... Discovering my personality
## ... Discovering the career that best fits me
```

Then the table of contents is easier to look at
![](28QqYFH.png)
