Click new page in gallery etc has custom layout

Create new page. At body, click New template:

![](XMsMVr4.png)

Then name your template and design the body. For example, a template I had named was “LOAD TEMPLATE”:
![](6tNF5U4.png)


To commit: Click modal background to commit. Now all future new pages you create in the Gallery, etc will have that template as an option. Imagine "LOAD TEMPLATE 1", "LOAD TEMPLATE 2", etc (not shown):
![](v2noUE1.png)


To cancel: If you change your mind, move this to Trash even though it’s not exactly a page:![](U9eIjsi.png)

---


But you probably want, when user clicks “New”, it’ll create using the template right away:

You do this by clicking the down caret on “New” button.
![](v4A5dDC.png)

Then setting your template as the default. The "..." will give you an option to set as the new default. This menu will flag whichever is the default
![](VRQH5Qj.png)
