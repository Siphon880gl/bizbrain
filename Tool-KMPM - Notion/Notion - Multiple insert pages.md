Type `/templ` and select the button option

Then you set up the trigger (when button is clicked) and actions (multiple actions of adding page) - you can ignore the formula's which names the dates incrementally:
![](Gj0c9Hn.png)


A button was pressed rather than creating each page manually and naming them manually
![](wAy1hPZ.png)
