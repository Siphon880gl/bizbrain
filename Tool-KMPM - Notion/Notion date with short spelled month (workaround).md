
Long spelled month:
January

Short spelled month:
Jan

Unfortunately as of 1/2025, Notion supports the long form only. But we have a workaround to force the short form spelling.

---

Formula to show long spelled month (which Notion easily supports):
```
formatDate(now(), "DD MMMM YYYY") # 10 January 2025
```

10 Jan 2025 (Yes it works and yes, Notion equations can support multilines)
```
formatDate(now(), "DD") + " " +   
if(formatDate(now(), "MM") == "01", "Jan",   
   if(formatDate(now(), "MM") == "02", "Feb",   
   if(formatDate(now(), "MM") == "03", "Mar",   
   if(formatDate(now(), "MM") == "04", "Apr",   
   if(formatDate(now(), "MM") == "05", "May",   
   if(formatDate(now(), "MM") == "06", "Jun",   
   if(formatDate(now(), "MM") == "07", "Jul",   
   if(formatDate(now(), "MM") == "08", "Aug",   
   if(formatDate(now(), "MM") == "09", "Sep",   
   if(formatDate(now(), "MM") == "10", "Oct",   
   if(formatDate(now(), "MM") == "11", "Nov", "Dec"  
))))))))))) + " " + formatDate(now(), "YYYY")
```

If have been incrementing the date by X number of days:
```
formatDate(dateAdd(now(), 1, "days"), "DD") + " " +   
if(formatDate(now(), "MM") == "01", "Jan",   
   if(formatDate(now(), "MM") == "02", "Feb",   
   if(formatDate(now(), "MM") == "03", "Mar",   
   if(formatDate(now(), "MM") == "04", "Apr",   
   if(formatDate(now(), "MM") == "05", "May",   
   if(formatDate(now(), "MM") == "06", "Jun",   
   if(formatDate(now(), "MM") == "07", "Jul",   
   if(formatDate(now(), "MM") == "08", "Aug",   
   if(formatDate(now(), "MM") == "09", "Sep",   
   if(formatDate(now(), "MM") == "10", "Oct",   
   if(formatDate(now(), "MM") == "11", "Nov", "Dec"  
))))))))))) + " " + formatDate(now(), "YYYY")
```


^ Might have to paste unformatted (CMD+SHIFT+V) because might paste as an inline code snippet (red text against gray background)

Bad:
![](OAEAv85.png)


^So go to the Formula mode:
![](bM547hu.png)


Bad - Incorrect (note red text is wrong, note it’s not in equation mode either):
![](3u9A6kN.png)

Correct: (paste unformatted AND formula icon on)
![](Gj0c9Hn.png)