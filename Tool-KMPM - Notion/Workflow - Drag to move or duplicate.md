
## Two modes
When dragging item from one area to another view, it moves it. 

But if you OPT+Drag, it’ll duplicate it. 

## Areas
- Page area to page area
  An example list dragging an item from a list that’s showing on the page to a table that’s showing on the same page
- Between sidebar and page
  Can apply to items dragged with or without OPT between sidebar and page