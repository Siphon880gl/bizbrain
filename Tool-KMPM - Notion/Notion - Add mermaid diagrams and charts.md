
In Notion, you can insert a diagram/chart using mermaid code.

---


Type `/mermaid` for a Mermaid code block:
![](68bV7Sn.png)

Type mermaid code:
```
pie   
    title Favorite features of Notion  
    "Databases": 42  
    "Organization": 30
    "Pages": 28
```

You may have to move to the top left to change the display mode (Code, Preview, Split)
![](esmw0lE.png)

And you'll get:
![](cPlSyBK.png)


----

```
graph TD
  Mermaid --> Diagram
```

Gets you:
![](sHZl94p.png)
