

![](Xcaep12.png)


Callout. Then change background color to default background:
![](lKhC6Ke.png)


You can choose not to have a left margin icon (In the above example is the three striped hamburger icon):
![](7YRHSy3.png)

^ And it would remove the left margin space too.

---

In-depth explanation:
[https://youtu.be/r0S2mozuH-0](https://youtu.be/r0S2mozuH-0)