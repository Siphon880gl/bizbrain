I use Obsidian with DataView and Breadcrumbs. I create DataView fields for representing Luhmann style numbering system. Then with Breadcrumbs I have multiple hierarchies so links have more meaning. For example, a hierarchy for specifying next & previous in series, up & down for branching, and related zettels

https://www.reddit.com/r/Zettelkasten/comments/xkrdkk/strategies_for_connecting_notes/