Leverage Obsidian's Graph View to see notes connecting to each other.

When reading about Zettelkasten online, zettel are notes, and Luhmann is the inventor of Zettelkasten.

When writing notes, make them atomic:
https://youtu.be/GtChrK4a-mc?t=181
- An atomic note should be understandable on its own without requiring extensive context from other notes. And this note note should be as brief as possible while still fully expressing the idea.

By meeting these criteria, you can connect other notes to/from. Then complex concepts may reveal themselves when you see how the atomic notes connect.

Form connections
https://youtu.be/GtChrK4a-mc?t=334
1. Make connections between existing notes
2. Make new note that connects two or more ideas


Finding new ideas (Depth technique)
https://youtu.be/GtChrK4a-mc?t=432
Depth technique, OR
Going into note and reading it


Depth?
![](NXsOmFd.png)

