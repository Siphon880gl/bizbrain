
How to create MOCs, when to cluster, mental squeeze point (your notes are so scattered you feel hopeless)

https://youtu.be/WUq8Pun28FI

Precede the filenames with a sorting format, and you also sort to keep related concepts clustered together if a folder doesn't make sense:
Dewey: 000-099 for one subject, 100 to 199 for another subject
Alternating: 0, 0a, 0a1, 1, 1a, 1a1

You can have a MOC of:
![](VVviVcX.png)
