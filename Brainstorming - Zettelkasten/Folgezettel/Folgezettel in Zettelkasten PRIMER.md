While the Zettelkasten method is a note-taking system that uses index cards or digital files to organize notes by topic, Folgezettel is a technique used within the Zettelkasten method to help users see how ideas were first connected when they were imported into the Zettelkasten. It can help users see which ideas have developed the most and what to write about next.

Namesake:
- Folge - sequence
- Zettel - snips/notes

---

A way to implement Folgezettel for Zettelkasten in Obsidian is through the Breadcrumbs plugin.
https://forum.obsidian.md/t/folgezettel-best-practice/29380

Using the **Breadcrumbs** plugin in Obsidian to implement the Folgezettel method is a creative and practical way to replicate the hierarchical and sequential structure of Zettelkasten's "follow note" system. Here's how you can set it up effectively:

### 1. Install and Configure the Breadcrumbs Plugin

- **Install** the Breadcrumbs plugin via the Obsidian Community Plugins section.
- Enable the plugin and open its **settings**.

### 2. Set Up "Before" and "After" Fields

- In the **Hierarchy Settings**, add fields like `before` and `after` to define the relationships between notes.
- Configure these fields in the Breadcrumbs settings as "Directed" relationships. This allows the plugin to understand the directional links (e.g., Note A → Note B → Note C).

### 3. Add Metadata to Your Notes

In each note's frontmatter, add the `before` or `after` fields to indicate the next or previous note in the sequence. For example:

#### Note A

```yaml
---
after: Note B
---
```

#### Note B

```yaml
---
before: Note A
after: Note C
---
```

#### Note C

```yaml
---
before: Note B
---
```

### 4. Enable the Breadcrumbs View

- Open the Breadcrumbs view pane in Obsidian to see the "before" and "after" relationships as a navigable structure.
- This pane will display the preceding and following notes based on the configured metadata.

### 5. Benefits and Limitations

- **Benefits**:
    - Simple and lightweight way to manage sequential relationships between notes.
    - Maintains the focus on local connections, consistent with Folgezettel principles.
    - Avoids overwhelming visualizations, focusing instead on the immediate context.
- **Limitations**:
    - You won’t see a complete lineage or hierarchy at a glance.
    - The "bird's-eye view" of the entire note structure requires additional tools like **Graph View**, but it won't show the specific sequence.

### 6. Optional Enhancements

- **Dataview Plugin**: Use Dataview queries to create a summary of linked notes based on the `before` and `after` relationships.
- **Templates**: Set up templates with pre-defined metadata fields for consistent note creation.
- **Combining with Tags or Folders**: Use tags or folders to categorize notes while maintaining the sequential Folgezettel structure.

This setup closely mimics the original Zettelkasten system's intent, focusing on sequential, contextual navigation rather than a sprawling, unordered graph of notes.

---


Here's a case study of how Breadcrumbs and using `before:NOTE` and `after:NOTE` YAML frontmatter properties worked to look back at notes to find Folgezettel's (Sequence of thoughts)

https://en.rattibha.com/thread/1614971744334258179

![](5GnSBjE.png)


![](qptvQMT.png)

