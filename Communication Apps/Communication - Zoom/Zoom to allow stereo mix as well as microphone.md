
Demonstrating an app or video over zoom while speaking into the mic?

Even if you have one input that picks up the computer playing loud audio while you're talking, Zoom will automatically filter out the computer's sound and isolate your listeners to only your voice.

**Enable sound at the Share Screen options:**
![](fzEj83D.png)

^Notice there's a "Share sound" option to the right.

---

**Proof that audio settings are ok:**

Worried about Audio settings? This setting is even acceptable (As long as Share sound is on):
![](iX3P4Yl.png)
