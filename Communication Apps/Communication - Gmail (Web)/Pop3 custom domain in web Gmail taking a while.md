
When you have a custom domain setup as POP3, it's possible you dont get emails right away. This could be problematic. To get email *now* especially when you're expecting it:

Go to your web Gmail settings -> Account and Imports
![](uaiMY8a.png)

Then click "Check mail now"
![](LL2NdUQ.png)


Timing:
- Typical delay is usually a few minutes to about 15 minutes.
- If the external account is quieter or after periods of inactivity, Gmail may reduce how often it checks, leading to delays that can approach an hour.
- Unfortunately, there’s no way to force Gmail to check more frequently—this polling frequency is determined automatically by Google’s system.