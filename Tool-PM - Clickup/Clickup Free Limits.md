_Workspaces are at the highest level of the ClickUp Hierarchy_. Spaces, Folders, Lists, and tasks all live within a Workspace.  


Workspaces
- Workspaces on our Free Forever Plan have a limit of 5 Spaces
- Workspaces on our Unlimited Plan and above have unlimited Spaces.

Lists and Folders: 
The number of Lists and Folders you can have per Space depends on your plan:

- **Free Forever Plan:** 100 Lists and 100 Folders per Space
- **Unlimited Plan:** 200 Lists and 200 Folders per Space
- **Business Plan:** 400 Lists and 400 Folders per Space
- **Business Plus Plan:** 1,000 Lists and 1,000 Folders per Space
- **Enterprise Plan:** Unlimited Lists and Folders per Spac

---

List Settings
- Custom Fields (for Task View's Details custom fields) - FREE
- Task statuses (aka Groups in Kanban Board View, aka Status Groups in List View) - FREE
- Automation - NOT FREE. $7/mo.
