You can manage calendar from a list

You can convert a list into a Calendar

![](zVG9EPl.png)

If Calendar not an option, click + View then select Calendar

Your tasks will be viewed as calendar style, with specific ranges eg 4pm-6pm or start or end or whole day (if no time specified)

---

See how it can look in Calendar:

![](W9DVSYk.png)

  

Set the time to have time range or a time point below task name in the Calendar:
![](QaklV0T.png)


The times can be set under Dates in either Task view or List view:
![](iXJEgP8.png)


![](UGoA1KS.png)


---


Collaboratively

We should tag our tasks with our names

If you tag all Weng’s tasks with tag weng and tag all John Doe’s tasks with tag johndoe. Then, for example seeing all Weng’s on Calendar can be:

![](EXYIwGB.png)

And without tag, all team members who created tasks can be viewed on the calendar