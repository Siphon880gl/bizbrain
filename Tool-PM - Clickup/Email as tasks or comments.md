
## **Send emails as tasks to list**
How? At sidebar, expand the space, hover mouse over the list item to review ellipsis “...” button. Click ellipsis button  → “Email to List” → Copy email address from popover modal

Email using the Basic Use instructions below

1:02


---


## **Send emails as comments to a task**
How? Open Task. In Task view. Click ellipsis “...” button next to Share → “Send email to task” Copy email address from popover modal

Email using the Basic Use instructions below

2:12

![](cuOXrYy.png)


The above two sections are sourced from:
[https://youtu.be/DZ96_DDrTvg](https://youtu.be/DZ96_DDrTvg)


---

## Basic Use

Email the list or task associated email address. Your task name is the subject line. Your task description is your email body (btw the task description will also mention your email address used).

Your subject line may contain special code to enhance the task with due date, assignees, tags, etc. Outside of the special code, your subject line contains your task name.


---

## Advanced use


#### Deadline
To add a deadline to a task when sending an email to a list in ClickUp, you can use the tag `<due date & time>` in the subject line. You can also use natural language to set the due date, such as `<due next Friday>` or `<due 20th of August 2021>`. To set a due time, you can use the syntax `<due mm/dd/yyyy at 2pm>`


#### Assign
- Using in subject line `<assign me>` assigns the task to the ClickUp user associated with the email address you are sending from.
- `<assign example@email.com>` assigns the task to the ClickUp user associated with the email address [example@email.com](mailto:example@email.com).

#### Tags
- Using in subject line `<tag ClickUp_tag_name>` tags an existing tag or tags then adds new tag. 
- Multiple tags? Subject: `Task Title <tag TagName1> <tag TagName2> <tag TagName3>`

#### Task Attachments
Just attach file as part of the email. Limit 10mb


The above are sourced from:
https://help.clickup.com/hc/en-us/articles/6309707288599-Create-tasks-and-comments-via-email

---

## How it looks like


I emailed the List email:

![](C1oNXsz.png)


After 1-2 mins, I see the task appears in the list!
![](kU3oM4q.png)

Opening the task, I see the tag, task name, and task description along with my email address
![](6pPsM40.png)

