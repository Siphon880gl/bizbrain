
### Basics

Clickup 3.0
Use cases: https://www.youtube.com/watch?v=9jsfFnR0YOs
For enterprise implementation: zenpilot.com they take your process and implement it in your clickup account

Free is limited to 5 workspaces
When starting a new workspace, you select views (eg. kanban for engineers, etc)


When you first join, there's an onboarding checklist (kinda like quest system):
Join a Workspace
Add a task
Add a comment
Create a View

### Hierarchy

- **Workspaces**: These are the highest organizational level in ClickUp, where you can manage multiple teams or projects. Each workspace is independent, with its own settings and members. The free plan typically limits the number of spaces to 5 (as of Q2 2024).
- **Spaces:** Think of as a channel or project. In a space you can have folders, lists, and docs
- **Folders, Lists, and Docs**: Within a space, you can organize work into folders, lists, and docs. Folders can contain multiple lists. Lists are where you actually add and manage tasks, and you can think of them as detailed breakdowns within a project or a specific workflow.
- **Views**: ClickUp allows you to customize how you view tasks and projects through different views like Kanban, List, Calendar, and more. Each view is designed to cater to different preferences and workflows, such as Kanban for visual workflow management or List view for a more traditional task list approach. **Confusingly, doc is an item in space, but doc is also a view for folder or list**

If the hierarchy is still confusing, think of Clickup helping you manage Tasks, Projects, and Initiatives, and watch this video:
https://www.youtube.com/watch?v=NriuVBLV50E


### Views

Of note, the bare minimum you may want to look at are List View, Board, Calendar, and Doc, and Mindmap

- **List View**: This is the standard view that displays tasks in a list format. It's ideal for seeing everything at a glance and for those who prefer a straightforward, itemized view of their tasks.
- **Board (Kanban) View**: This view visualizes tasks on a board, allowing users to drag and drop tasks between different status columns. It's great for workflow visualization and managing tasks in a highly visual format.
- **Calendar View**: This view shows tasks on a calendar, making it perfect for planning and visualizing deadlines, scheduling, and understanding how tasks are distributed over time.
- **Gantt View**: Gantt charts in ClickUp help with project planning and tracking progress against deadlines. You can visualize task dependencies, durations, and the overall project timeline.
- **Box View**: This view provides a high-level overview of workload and progress. It's useful for managers to see what everyone is working on and how tasks are distributed among team members.
- **Table View**: This view presents tasks in a spreadsheet-like format, offering a comprehensive overview and allowing for easy data manipulation and analysis.
- **Timeline View**: Similar to Gantt but with a focus on time allocation, the Timeline view helps you see tasks and projects over time, understand scheduling, and manage resources effectively.
- **Workload View**: This view is designed to help you understand team capacity, visualize who is over or underloaded with work, and redistribute tasks as necessary.
- **Doc View**: In this view, you can create, edit, and collaborate on documents. It integrates seamlessly with other views, allowing you to link tasks, embed views, and more.
- **Mind Maps**: Mind Maps in ClickUp allow you to visually brainstorm, plan projects, and organize ideas or tasks in a free-form, intuitive way.
- **Activity View**: This provides an overview of all activity in a space, folder, or list, offering insights into what tasks are being worked on and recent updates.


### Dashboards: 


While not a traditional "view" of tasks, Dashboards in ClickUp allow users to create custom overview pages using various widgets to track progress, metrics, and other key data points. 

The actual term used by Clickup are cards: You organize cards on a page to make up your dashboard (eg. a bar chart card); You can resize the cards

Obligated:
https://www.youtube.com/watch?v=dDL2px6_wq8


