On a company page, if you have Sales Navigator access or you have ever tried Sales Navigator (then LinkedIn gives a sneak peak a few decision maker connections):
![](bqDSE9h.png)

---

If need help on searching companies to go on a company page that has this information, please refer to [[LinkedIn Fundamentals - Search for company]]