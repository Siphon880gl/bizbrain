As of 1/2025:
You might not get this upsell. Sales Navigator you can try one month, and it’s $139.99/month

**Price: ~~$139.99~~* 1-month free trial**

After your free month, pay as little as $139.99* / month (save 22%) when billed annually. Cancel anytime. We'll remind you 7 days before your trial ends.