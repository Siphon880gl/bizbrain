```toc
```

**How to use**: Use table of contents. This tutorial is in prompt style. Bolded blocks of text is the prompt, otherwise is the response.

**In the future**: Will make look nicer in the future

## Strategy

**If we outreached random people to try our app (a link) and they don’t open our message, does that make our domain get marked as spam to Gmail, Microsoft, etc? All subdomains from wengsapp.ai would automatically be marked as spam for all Gmail users too, right?**

**Then is it a good idea to buy another domain like getwengsapp.com, which redirects to wengsapp.ai? Then we use that domain for emailing until it gets marked as spam, then we buy another domain for emailing like trywengsapp.com?**

**And this approach would prevent our official url wengsapp.ai from being permanently marked as spam?**

**And are new domains automatically suspect to Gmail? So we have to pretend to email authentic people who replied back, before that new domain won’t be marked as spam? How many days of back and forth emails before a domain is trusted? I supposed there are online services that simulate the back and forth emails for us to warm up the domain to be trustworthy by Google, Microsoft, etc email services.**

**And is it true Google looks for mass emailing and limits how many emails we can send out at a domain a day or an hour, before permanently marked as spam to all Gmail users? If that’s the case, does that mean having to buy multiple domains just for emailing? And if user visits the domain, it should redirect to our main brand website at wengsapp.ai**

**From the above concerns, mostly to make sure our email doesn’t automatically go to spam so potential leads can even know to click the link to try our app - give us a plan.**

**I see some highlights are using multiple domains and subdomains that we rotate email to email, using email warm up services, not using the word FREE!! in subject line, having an unsubscribe link, setting up SPF/DKIM/DMARC, testing send to emails with verifier services Use tools provided by Google and Microsoft themselves to track domain’s reputation. Use tools that rotate IP to avoid blacklisting**

  

---

  

Email deliverability is critical for outreach, and getting flagged as spam can harm both your reputation and your business. Here's a plan to mitigate these risks and ensure your emails land in the inbox, not the spam folder:

---

### **1. Understand Email Deliverability Risks**

#### Key Risks:

- **High Spam Reporting:** Random outreach to unengaged recipients increases the chance of being flagged as spam.
- **Domain Reputation:** Spam flags on one domain (or subdomain) can harm your primary brand domain.
- **Spam Traps:** Sending emails to invalid or inactive addresses can trigger spam systems.
- **Unverified New Domains:** Email providers view new domains as suspicious until they establish a positive reputation.

---

### **2. Adopt Best Practices for Email Outreach**

#### **A. Use a Dedicated Email Domain**

- Register a domain like `getwengsapp.com` for outreach emails to protect the reputation of your main domain (`wengsapp.ai`).
- Use a subdomain (e.g., `email.wengsapp.ai`) if you'd like to keep branding consistent but isolate email reputation.

#### **B. Warm Up the New Domain**

- Warm up the domain by sending a small volume of emails first, increasing gradually over weeks.
- Engage with real recipients who reply to your emails. If you don’t have an audience for this, use a domain warming service (like Warmbox or Mailreach) to simulate authentic email engagement.

#### **C. Optimize Email Content**

- Personalize emails to make them relevant to recipients.
- Avoid spammy language (e.g., “FREE!!!” or excessive exclamation marks).
- Always include a clear and easy-to-find **unsubscribe link**.
- Use professional email authentication standards:

- **SPF, DKIM, and DMARC:** Ensure your emails are authenticated to avoid spam filters.

---

### **3. Monitor Email Volume and Sending Patterns**

- **Limit Mass Emails:** Gmail and Microsoft monitor email volume. Sending too many emails too quickly can lead to temporary or permanent blocks.

- Start with 50–100 emails/day and increase gradually.
- Avoid exceeding 500 emails/day for new domains in the first few weeks.

- **Throttle Emails:** Space out emails over time instead of sending them all at once.

---

### **4. Diversify Email Domains**

If you’re scaling email outreach:

- Rotate between multiple domains (e.g., `wengsapp.ai`, `trywengsapp.com`) to spread out the risk.
- All domains can redirect to your main website `wengsapp.ai` for consistency.

---

### **5. Build a Trustworthy Sender Reputation**

#### **Improve Engagement Rates**

- Target qualified leads using clean, verified email lists. Use tools like Hunter.io or NeverBounce to verify email addresses.
- Focus on engagement by sending emails to people likely to open and reply. Low engagement signals spam behavior to providers.

#### **Engage with Responders**

- Prompt responses build trust with email providers. After 2–4 weeks of engagement, your domain will be seen as trustworthy.
- Domain warming tools can simulate this if you don’t have organic responses.

---

### **6. Track and Optimize**

#### Monitor Reputation:

- Use tools like Google Postmaster Tools and Microsoft SNDS to track your domain’s reputation.
- Watch for sudden drops in deliverability or bounce rates.

#### Email A/B Testing:

- Test variations in subject lines, email content, and sending times to improve open rates and engagement.

---

### **7. Manage Volume with Email Services**

- Use an email service provider (ESP) like Mailgun, Postmark, or SendGrid to handle the technical aspects of email sending. These services:

- Manage SPF/DKIM/DMARC for you.
- Offer IP rotation to avoid blacklisting.
- Provide analytics for email performance.

---

### **Summary (Plan):**

1. **Set Up a Dedicated Domain** for outreach, separate from `wengsapp.ai`.
2. **Warm Up the Domain** through gradual email sending and engagement (real or simulated).
3. **Send Personalized Emails** to verified, targeted leads with clear opt-outs.
4. **Monitor Deliverability** using tools like Google Postmaster and SNDS.
5. **Diversify Domains** if scaling outreach significantly to avoid throttling or reputation issues.
6. **Consider Domain Rotation** and redirect outreach domains to `wengsapp.ai`.

This approach minimizes the risk of being flagged as spam and protects your brand's main domain reputation. Let me know if you'd like guidance on implementing specific tools or services!


---

---


## Strategy Highlights

**I see some highlights are using multiple domains and subdomains that we rotate email to email, using email warm up services, not using the word FREE!! in subject line, having an unsubscribe link, setting up SPF/DKIM/DMARC, testing that the target email addresses are valid with verifier services. And other highlights include using tools provided by Google and Microsoft themselves to track domain’s reputation, and using tools that rotate IP to avoid blacklisting**  

  

Here's a detailed breakdown of these strategies and tools to implement them effectively:

---

### **1. Use Multiple Domains and Subdomains**

- **Purpose:** Isolate outreach campaigns to protect your main domain's reputation.
- **How:**

- Use a dedicated domain like `getwengsapp.com` or subdomains like `outreach.wengsapp.ai`.
- Rotate between multiple domains or subdomains (e.g., `trywengsapp.com` and `invitewengsapp.com`) to distribute email volume and reduce the risk of blacklisting.
- Redirect all outreach domains to your main site, `wengsapp.ai`, for consistency.

---

### **2. Use Email Warm-Up Services**

- **Purpose:** Build trust with email providers and establish a good sender reputation for new domains.
- **How:**

- Use domain warming tools like:

- **Warmbox, Mailreach, Lemwarm, or Instantly.**

- These tools automate the process by:

- Sending small batches of emails daily.
- Simulating replies to your emails (engagement signals to email providers).

- Gradually increase your email volume over 4–6 weeks to establish domain trust.

---

### **3. Avoid Spammy Language**

- **Purpose:** Prevent spam filters from flagging your emails.
- **How:**

- Avoid words like "FREE!!", "Act now!", "Click here!", and excessive capitalization.
- Keep subject lines short, clear, and relevant.
- Use tools like **MailTester** or **Litmus** to analyze your email for spam triggers before sending.

---

### **4. Include Unsubscribe Links**

- **Purpose:** Ensure compliance with anti-spam laws (e.g., CAN-SPAM, GDPR).
- **How:**

- Add a visible unsubscribe link in every email.
- Use an ESP (e.g., Mailgun, SendGrid) to handle unsubscribes automatically.

---

### **5. Set Up SPF, DKIM, and DMARC**

- **Purpose:** Authenticate your emails to avoid spam filters and prevent spoofing.
- **How:**

- Work with your domain registrar or hosting provider to set up these records:

- **SPF:** Specifies which mail servers are allowed to send emails on your behalf.
- **DKIM:** Adds a digital signature to verify email authenticity.
- **DMARC:** Provides instructions to email providers on how to handle unauthorized emails.

- Test your setup using tools like **MxToolBox** or **DMARC Analyzer**.

---

### **6. Verify Email Addresses**

- **Purpose:** Avoid sending emails to invalid or fake addresses, which harm deliverability.
- **How:**

- Use email verification tools like:

- **NeverBounce, ZeroBounce, or Hunter.io.**

- Verify email lists before outreach to reduce bounces and spam trap hits.

---

### **7. Use Google and Microsoft Tools**

- **Purpose:** Monitor and improve domain reputation.
- **How:**

- **Google Postmaster Tools:**

- Monitor email performance (spam rate, delivery errors).
- Check your domain’s reputation with Gmail.

- **Microsoft SNDS (Smart Network Data Services):**

- Monitor your IP reputation for emails sent to Outlook/Hotmail users.

- Use these tools to adjust your strategy if your domain reputation declines.

---

### **8. Rotate IP Addresses**

- **Purpose:** Prevent IP blacklisting and maintain good deliverability.
- **How:**

- Use an ESP like **Mailgun**, **Postmark**, or **SendGrid** that offers:

- **Shared IP Pools:** Suitable for small senders.
- **Dedicated IPs:** For higher volumes, where you control the reputation.

- Some services rotate IPs automatically to avoid overloading a single IP address.

---

### **9. Track and Analyze Email Campaigns**

- **Purpose:** Optimize your outreach and fix deliverability issues.
- **How:**

- Use analytics from your ESP to monitor:

- Open rates.
- Click-through rates.
- Bounce rates.

- Adjust campaigns based on engagement data.

---

### **10. Limit Email Volume**

- **Purpose:** Avoid triggering spam filters by sending too many emails at once.
- **How:**

- Start with **50–100 emails/day** for new domains and gradually increase.
- Avoid exceeding **500 emails/day per domain** initially.
- Use tools like **Gmass** or **Instantly** to throttle sending and schedule emails.

---

### **Recommended Tools**

#### Domain Setup and Verification:
- **MxToolBox, DMARC Analyzer, SPF Record Generator**
#### Email Warm-Up:

- **Warmbox, Mailreach, Lemwarm, Instantly**

#### List Verification:
- **Hunter.io, NeverBounce, ZeroBounce**
#### Campaign Management:
- **Mailgun, SendGrid, Postmark, Gmass**
#### Monitoring:
- **Google Postmaster Tools, Microsoft SNDS**

---

### **Summary (Strategy Highlights)**

1. **Protect Your Main Domain:** Use separate domains or subdomains for outreach.
2. **Warm Up Your Domain:** Gradually increase email volume with a warm-up tool.
3. **Authenticate Emails:** Set up SPF, DKIM, and DMARC.
4. **Send Verified Emails:** Clean your list using email verification tools.
5. **Optimize Content:** Avoid spammy language, include an unsubscribe link, and personalize emails.
6. **Monitor Reputation:** Use Google and Microsoft tools to track domain and IP health.
7. **Scale Safely:** Rotate domains, limit volume, and use ESPs for IP rotation.

This approach minimizes risks while maximizing your outreach impact. Let me know if you'd like assistance with specific tools or technical configurations!

---
---

## SPF, DKIM, DMARC

**Can services like Smartlead help handle SPF, DKIM, DMARC?**

Services like **Smartlead** and other email outreach platforms can assist with **SPF**, **DKIM**, and **DMARC** setup, though their involvement depends on how their platform integrates with your domain and email sending process. Here's a breakdown:

### **How Smartlead (or Similar Services) Can Help:**

#### **1. Simplified Setup for SPF, DKIM, and DMARC**

- **SPF:** They typically guide you to add an SPF record to your DNS settings that authorizes their mail servers to send emails on your behalf.
- **DKIM:** They provide you with a DKIM key to add to your DNS records, which signs your emails for authentication.
- **DMARC:** While they may not directly enforce DMARC, they can provide the required DNS entry and advice on its configuration to monitor or enforce policies.

#### **Smartlead’s Role:**

- Many email outreach platforms include clear instructions, pre-generated DNS records, and a dashboard to verify that your SPF, DKIM, and DMARC are correctly configured.
- Their support team often assists in troubleshooting if authentication isn’t set up properly.

---

### **2. Automated Domain Warm-Up**

- Services like Smartlead integrate domain warm-up tools that not only build trust for your domain but also ensure your authentication records are recognized by major providers like Gmail and Outlook.
- They simulate real engagement, which helps boost your sender reputation and ensures that SPF/DKIM/DMARC configurations work as expected.

---

### **3. Integration with ESPs (Email Service Providers)**

- Platforms like Smartlead integrate with popular ESPs (e.g., SendGrid, Postmark) or SMTP services.
- They streamline the process of setting up your domain’s authentication records in alignment with these providers.

---

### **4. Ongoing Monitoring**

- Many services provide built-in analytics to monitor your sender reputation, bounce rates, and other deliverability metrics, ensuring your authentication settings are functioning correctly.

---

### **What They Cannot Do Directly:**

- They **cannot directly manage your DNS records** (e.g., adding SPF/DKIM/DMARC records). You’ll need access to your domain registrar or hosting provider to implement their instructions.
- They **cannot guarantee domain reputation** if other risky practices (like sending to unverified lists) are followed.

---

### **Summary (SPF, DKIM, DMARC)**

Yes, Smartlead can help you handle SPF, DKIM, and DMARC by providing:

- Pre-configured records to add to your DNS.
- Verification tools to ensure they’re set up correctly.
- Domain warm-up services to build trust.

However, you’ll need access to your DNS management to implement their recommendations. Once set up, they’ll ensure smooth operation and provide ongoing deliverability support.

---
---

## TLDs (Top Level Domains) Matter

**Does it get more likely marked as spam for other tld like .info?**

Yes, certain top-level domains (TLDs), like `.info`, are more likely to be flagged as spam compared to more established or widely used TLDs (e.g., `.com`, `.org`, `.net`). This happens because:

### **1. Spam Reputation of TLDs**

- **.info TLD:** Historically, `.info` domains have been cheaper and more commonly used by spammers for phishing or mass email campaigns. As a result, email providers like Gmail and Outlook may be more cautious with emails from `.info` domains.
- **Higher Trust for .com and .org:** Popular TLDs like `.com`, `.org`, and even some country-specific TLDs are perceived as more legitimate due to their widespread use by established businesses.

---

### **2. Email Provider Heuristics**

Email providers use algorithms to analyze the trustworthiness of an email sender. A domain's TLD is just one factor, but for less-trusted TLDs, even small issues (e.g., poor engagement, lack of SPF/DKIM/DMARC) can result in higher spam likelihood.

---

### **3. Other Key Factors**

Even if you use a `.info` domain, you can mitigate spam risk with:

- **SPF/DKIM/DMARC:** Proper authentication makes your domain more trustworthy.
- **Domain Age and Reputation:** Older, well-maintained `.info` domains with no spam history may perform better.
- **Engagement Metrics:** High open/reply rates and low complaint rates signal legitimacy.
- **Email Volume and Consistency:** Avoid sending too many emails at once, especially from a `.info` domain.

---

### **Best Practices**

If you're concerned about spam risks:

1. **Choose a Trusted TLD:** Use a `.com`, `.net`, or `.org` domain for email campaigns.
2. **Authenticate Your Emails:** Set up SPF, DKIM, and DMARC.
3. **Warm Up Your Domain:** Gradually build trust with email providers through domain warming.
4. **Monitor Reputation:** Use tools like Google Postmaster Tools and Microsoft SNDS to track your domain and IP health.
5. **Use Established ESPs:** Use services like Mailgun or SendGrid for their built-in reputation management.

---

### **Should You Avoid .info?**

If you're starting fresh and concerned about reputation, it’s safer to use a `.com` or other widely trusted TLD. While you can work around `.info`’s inherent challenges, choosing a trusted TLD simplifies email deliverability efforts.

---
---

## TLDs Trustworthiness

**what are some TLD's to avoid when starting out and trust is important? Rank them from worse to best**

When starting out and building trust is critical, some TLDs should generally be avoided due to their poor reputation or association with spam and malicious activities. Below is a ranked list of TLDs, from **worst to best** for trustworthiness, with notes on why they rank where they do:

---

### **TLDs to Avoid (Worst to Better)**

#### **1. .tk (Tokelau)**

- **Reputation:** Widely abused by spammers and scammers because it's free to register.
- **Trust Level:** Extremely low; emails from `.tk` domains are often flagged as spam.

#### **2. .ml (Mali), .ga (Gabon), .cf (Central African Republic), .gq (Equatorial Guinea)**

- **Reputation:** These are also free or very cheap to register and have been heavily abused for phishing and scams.
- **Trust Level:** Very low; often flagged as suspicious by email providers.

#### **3. .info**

- **Reputation:** Cheap and previously targeted for abuse, particularly in phishing schemes and spam campaigns.
- **Trust Level:** Moderate risk; better than the free domains above but still faces deliverability challenges.

#### **4. .biz**

- **Reputation:** Associated with low-budget or spammy marketing campaigns.
- **Trust Level:** Moderate; often seen as less credible than `.com` for businesses.

#### **5. .xyz**

- **Reputation:** Cheap and popular among startups and creative projects, but also used for malicious purposes in the past.
- **Trust Level:** Mixed; gaining some legitimacy but still often treated with caution by email providers.

#### **6. .pw (Palau)**

- **Reputation:** Commonly used in spamming and low-cost campaigns.
- **Trust Level:** Moderate to low; viewed as less professional.

#### **7. .work, .click, .loan, .top**

- **Reputation:** Cheap and often associated with high-risk industries or spammy practices.
- **Trust Level:** Mixed to low; use with caution.

#### **8. .me**

- **Reputation:** More legitimate and used for personal branding, but occasionally flagged as suspicious due to prior abuse.
- **Trust Level:** Moderate to good; better suited for personal projects than businesses.

#### **9. .co**

- **Reputation:** A legitimate alternative to `.com`, often used by startups. However, it’s sometimes flagged as a typo for `.com` and associated with phishing attempts.
- **Trust Level:** Good but slightly riskier than `.com`.

#### **10. Country-Specific TLDs (e.g., .ru, .cn, .ng)**

- **Reputation:** Varies widely by country. Some (e.g., `.ru` and `.cn`) are flagged more often due to abuse from those regions.
- **Trust Level:** Depends on the country, but international recipients may treat some as suspicious.

---

### **Recommended TLDs for Trustworthiness (Best Options)**

#### **1. .com**

- **Reputation:** The most trusted and widely recognized TLD for businesses and professionals.
- **Trust Level:** Excellent; the gold standard for credibility.

#### **2. .org**

- **Reputation:** Associated with non-profits and organizations, often viewed as trustworthy.
- **Trust Level:** Excellent, though not ideal for purely commercial purposes.

#### **3. .net**

- **Reputation:** A solid alternative to `.com`, commonly used for tech or networking companies.
- **Trust Level:** Very good; second only to `.com`.

#### **4. .edu**

- **Reputation:** Restricted to educational institutions, ensuring high trust.
- **Trust Level:** Excellent, though not available for general use.

#### **5. .gov**

- **Reputation:** Restricted to government entities, making it highly trustworthy.
- **Trust Level:** Excellent, though not available for businesses.

#### **6. .ai**

- **Reputation:** Increasingly popular among tech companies and AI-focused startups. Trusted but still niche.
- **Trust Level:** Very good for tech-focused businesses.

#### **7. Country-Specific TLDs with Strong Regulation (e.g., .uk, .ca, .de, .us)**

- **Reputation:** Well-regulated and associated with businesses or organizations in those regions.
- **Trust Level:** High for local audiences, good for international audiences.

---

### **General Advice**

- **Avoid overly cheap or free TLDs.** They are often abused and flagged as spam.
- **Stick to widely recognized and trusted TLDs.** `.com` remains the best option, but `.org`, `.net`, `.ai`, and well-regulated country TLDs are also good choices.
- **Use branding-friendly TLDs with caution.** TLDs like `.me`, `.co`, or `.io` can work well but may face initial skepticism.

By choosing a high-trust TLD and following email best practices (SPF/DKIM/DMARC, warm-up, etc.), you can maximize your email deliverability and credibility.

---

Weng's Personal ChatGPT: [https://chatgpt.com/share/678a2ddc-12d0-800f-9741-baeb68eb622c](https://chatgpt.com/share/678a2ddc-12d0-800f-9741-baeb68eb622c)

