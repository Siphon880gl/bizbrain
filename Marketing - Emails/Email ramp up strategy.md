
## Per email address (aka email inbox)

![](Vnx2Vxm.png)

Here's the updated graph showing the ramp-up plan starting at **30 emails per account per day**:

1. **Blue Line (Total Emails):** Total number of emails sent daily from all 20 accounts combined.
2. **Orange Dashed Line (Emails per Account):** Number of emails sent daily per individual account.
3. **Red Dashed Line:** Maximum recommended limit by Google Workspace for 20 accounts (2,000 emails/account/day).

### Explanation:

- **Days 1-5:** Start at 30 emails/account/day, totaling 600 emails/day for 20 accounts.
- **Days 6-10:** Gradually increase to 50 emails/account/day (1,000 emails/day total).
- **Days 11-15:** Increase to 100 emails/account/day (2,000 emails/day total).
- **Days 16-20:** Double to 200 emails/account/day (4,000 emails/day total).
- **Days 21-25:** Ramp up to 400 emails/account/day (8,000 emails/day total).
- **Days 26-30:** Cap at 800 emails/account/day (16,000 emails/day total).

This gradual increase ensures you avoid being flagged as spam and helps maintain a healthy domain reputation. ​

---

## Per Domain

To keep your domain's email reputation intact and avoid being marked as spam, the number of email inboxes (accounts) per domain should be balanced with the **volume of emails sent** and **domain reputation management practices**. There is no single fixed number, but here are key considerations and best practices:

---

### Key Factors Impacting Safety

1. **Volume Per Inbox:**
    
    - Each inbox should send a **low-to-moderate number of emails daily**. A safe benchmark is **30–50 emails per inbox per day during the warm-up phase**, scaling to **200–500 emails per day per inbox** depending on the domain's reputation.
2. **Total Emails Per Domain:**
    
    - Keep total emails sent from all inboxes on a domain below **5,000–10,000 emails per day** unless your domain is well-warmed and has excellent reputation scores.
3. **Domain Reputation:**
    
    - Gmail, Outlook, and other email providers monitor the reputation of your domain. If multiple inboxes on a domain are flagged as spam, it can affect all inboxes under that domain.
    - **Engagement rates** (opens, clicks) and **low spam complaints** are crucial to maintaining a healthy reputation.
4. **Sending Behavior:**
    
    - Avoid sudden spikes in email volume. Gradually ramp up the number of emails sent per inbox and across the domain.
    - Use **SPF**, **DKIM**, and **DMARC** authentication protocols to establish trustworthiness.
5. **Segmentation by Subdomains:**
    
    - Consider using **subdomains** for outreach (e.g., `outreach.example.com` or `campaigns.example.com`) instead of your primary domain. This isolates the main domain from potential reputation issues.

---

### General Rule of Thumb for Email Inboxes Per Domain

- **Low-Volume Outreach:**
    - If you're sending up to **1,000 emails/day**, 5–10 inboxes per domain are sufficient.
- **Medium-Volume Outreach:**
    - For **5,000–10,000 emails/day**, use 20–30 inboxes per domain.
- **High-Volume Outreach:**
    - For **10,000–20,000 emails/day**, spread the volume across **multiple domains** with 20+ inboxes per domain.

---

### Safe Scaling Strategy

1. **Warm-Up Phase:**
    
    - Start with **5–10 inboxes per domain**, sending a maximum of **30–50 emails per inbox daily**.
    - Gradually increase volume over 2–4 weeks.
2. **Long-Term Scaling:**
    
    - For each inbox, limit daily emails to **200–500/day** depending on your engagement rates and domain reputation.
    - If scaling further, add **additional domains or subdomains** to spread the volume.

---

### Subdomains for Hd igh-Volume Emailing

Using subdomains helps to:

- Isolate outreach campaigns from your primary domain.
- Ensure that spam complaints or reputation issues don't affect your main domain's email operations.
- Example:
    - Primary domain: `example.com` (for regular business communication).
    - Subdomain: `outreach.example.com` (dedicated to outreach campaigns).

---

### Monitoring Domain and Inbox Health

- Use tools like:
    - **Google Postmaster Tools:** To monitor Gmail reputation.
    - **MxToolbox:** To check domain health and blacklisting.
    - **Sender Score (by Validity):** To track IP and domain reputation.

---

### Example Setup for High-Volume Outreach

- **Goal:** 10,000 emails/day.
- **Recommended Setup:**
    - 2–3 domains (e.g., `example.com`, `example.net`, `example.org`).
    - 10–20 inboxes per domain.
    - Each inbox sending 100–500 emails/day (gradually ramped up).

By spreading the email load across multiple domains and inboxes, you reduce the risk of being flagged as spam and maintain a healthy domain reputation.