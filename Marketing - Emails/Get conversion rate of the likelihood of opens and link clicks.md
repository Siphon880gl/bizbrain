As a general practice of getting conversion rates for your industry:

Google their industry average conversion rate. Eg. Google Search: "walmart industry average conversion rate"
Eg. For e-commerce, it’s usually 2.5% to 3%

If Google isn’t giving good results.

Use the free Perplexity AI for asking industry conversion rate. If it’s not confident, then say it can give a reasonable range.