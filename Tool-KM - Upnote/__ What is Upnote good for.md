KM - Some limited Knowledge Management because has notebooks, saved search keywords, and tags

But is better for fleeting thoughts and if you have a queue of tasks that need to be done in order:
- Fleeting thoughts: Easily create a new note at "All Notes" mode, then come back to it to finalize the note into another app or a notebook.
- Queue of tasks that need to be done in order: Updating notes (with a space or Enter) to make them appear to the top (sorted by modified) if you have a queue of tasks that need to be done in order.

Zettelkasten friendly? 
- Yes, but cannot easily find Orphaned thoughts like Obsidian's Graph View can.