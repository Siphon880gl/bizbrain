
Purpose: When you type / and you can start typing what feature you want, it's either collapsible or toggleable depending on the app. Here's mnemonics:

UpNote -> CupNote -> Collapsible

Notion -> NoTog -> Toggleble list