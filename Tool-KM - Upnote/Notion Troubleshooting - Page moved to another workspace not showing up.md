Problem: Page moved to another workspace not showing up.

At the other workspace you can’t find the newly duplicated, but when you go Search, you can find it. If you click "Home", you might also find it on the home's page
![](Z0qQrc6.png)

---

Solution:
1. Open the note with any of the above means (Search or Home)
2. Then you can move it to another area of the left side menu
   ![](azZfnEc.png)
3. Then finally, inside the left side menu, drag the item to the desired position