
Queue of tasks that need to be done in order: 
- Updating notes (with a space or Enter) to make them appear to the top (sorted by modified) if you have a queue of tasks that need to be done in order.
- If you're not using Pinning feature, you can pin the notes as a way to move them to the top and treating the top area as a queue or todo list. However if you've been using pins already, this is not a viable approach (then you have to rely on updating notes and then using the sorted by last modified)

---

Disadvantage the queuing next tasks by sorting modified by introducing space or enter on notes to move them to the top of the notes list and that top area becomes your  temporary todo list by sorting by last modified:
- You may find yourself updating previous notes and titles (like the way Upnote is used for - to have notes) and they will pop to the top, detracting you from using the top area notes as todo list
- You can have a naming convention for those notes

( means just ignore
(1x means ignore for now
(2x means ignore for now and next time (because you have something planned)
(0x means you don’t know when exactly this task will be relevant but it will be when it’s time to 
do that project


For example, here Im focused on immediately writing this tutorial and any business related tasks (Be on-call this weekend and attend to my startup Videolistings). And all non-business related tasks like improving my productivity system on Notion and my own personal startup, those are potentially put off two times (maybe today and tomorrow, however long each phase of activities take, and one of the phases is getting to business). I had to denote to ignore because they had to pop up near the top of the notes. You could modify the two relevant notes to make them appear even higher, but that wastes time - up to your discretion.

![](VqVMiVd.png)
