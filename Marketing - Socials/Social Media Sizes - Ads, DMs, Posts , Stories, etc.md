Aka for searching: Dimensions

## Ads
https://www.facebook.com/business/ads-guide/update/image/instagram-feed/link-clicks

## DMs
### Instagram DMs
Not much resources on the preview thumbnails created in instagram DMs. TODO: I'll look into the exact dimensions by creating gridlined preview graphics to see how they look in DM.

Mobile Instagram DM:
Transparent GIF is respected and lets the black come through. Square dimensions are respected
![](9T0aGjp.png)


Desktop Instagram DM:
Transparent GIF's alphas are colored gray. Square dimensions are NOT respected and is cropped. Instead of 1:1, it looked like .5 width to 1 height, equally cropped from top and bottom.

The aspect ratio is .5:1 on Desktop DM and 1:1 on Mobile DM so you have to add bars to the left and right for the thumbnail to look good on both devices.
![](oDpdunK.png)


## Posts, etc

https://www.socialpilot.co/blog/social-media-image-sizes

https://buffer.com/library/instagram-image-size/

https://blog.hootsuite.com/social-media-image-sizes-guide/#Instagram_image_sizes


---

You can also search templates on Canva, Adobe Express, etc, and it'll be dimensioned correctly.