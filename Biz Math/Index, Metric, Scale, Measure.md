## Synonyms

index, metric, scale, measure

## Index around 1

A metric where 1 is considered the ideal value and the metric itself can be less than or greater than 1 sounds like a ratio or a scale where 1 represents a specific reference point or optimal condition. This kind of metric doesn't have a universal name, but it could be referred to in several ways depending on its application:

1. **Ratio**: In general terms, this kind of metric can be called a ratio, especially if it's the result of dividing one quantity by another. The value of 1 in a ratio indicates that the numerator and denominator are equal, representing a balanced or ideal state.
    
2. **Index**: When such a metric is used to measure a specific phenomenon or outcome, it may be called an index. An index value of 1 might represent an equilibrium, target, or optimal level, with values below or above 1 indicating deviations from this target.
    
3. **Benchmark**: The term benchmark is often used when the metric is compared against a standard or desired value. If the metric is a benchmark, then 1 represents the achievement of the benchmark, with values below or above indicating underperformance or overperformance relative to the benchmark.
    
4. **Relative Scale**: This term could be used when the metric is relative to a particular reference point. A value of 1 on a relative scale signifies alignment with the reference point.
    
5. **Coefficient**: In some contexts, especially in statistics or scientific measurements, such a metric might be called a coefficient. A coefficient value of 1 often indicates a specific baseline or standard condition.
    

The specific naming and interpretation would depend on the field of application and the context in which the metric is used. For example, in finance, a ratio of 1 might indicate a perfect balance between two financial metrics. In contrast, in medicine, a value of 1 might indicate a baseline health condition or risk level.

---

Let's take a look at the T.I. or transparency index. It measures how transparent versus privacy intrusive an app is. 

If the intrusiveness is greater than transparency you have a low metric which is likely to offend or scare people and put them off your app. If high or reasonable index, because you disclose what privacy information is tracked (no obscuring it) and/or not much is tracked, then people are more likely to download or adapt your app. 

![](0zdStpk.png)
^ The above from [https://growth.design/case-studies/apple-privacy-policy](https://growth.design/case-studies/apple-privacy-policy) about another topic

In that diagram the index is calibrated or designed around the number 1 (imagine the diagonal line starts at 0,0). If greater than 1, there is more transparency than intrusiveness, and that is desirable to attract and retain users. If less than 1, than it's more intrusive than transparent.


---

## Other Calibrations

The TI is not always scaled to be below or above 1. Some are scaled to a maximum value of 1 (or 100, if expressed in percentage terms). This scale means that a value of 1 (or 100%) represents the highest possible level of transparency. 

However, there could be specific contexts or models where the transparency index is defined differently, allowing values greater than 1. Here's how that might work in various contexts:

1. **Normalized Scale**: Normalization is a statistical method that adjusts values measured on different scales to a notionally common scale. When an index is normalized to a scale of 0 to 1 or 0 to 100, it means that the lowest possible value is represented by 0 (or 0%), and the highest possible value is represented by 1 (or 100%). This makes it easier to interpret the data at a glance and compare across different datasets or variables.
    
2. **Percentage Scale**: Often, a 0 to 100 scale is simply a percentage representation of the 0 to 1 scale. It's the same data, but multiplied by 100 for a more intuitive understanding. For example, a 0.75 score on a 0 to 1 scale is the same as 75 on a 0 to 100 scale.

3. **Custom Scale**: The index could be designed on a scale where the maximum value is greater than 1, making values above 1 possible and indicating exceptionally high transparency levels.
    
4. **Relative Measure**: If the index is relative, values greater than 1 could indicate performance that exceeds a certain benchmark or average.
    
5. **Composite Metrics**: If the transparency index is a composite metric aggregating various factors, the summation could exceed 1 if not normalized.