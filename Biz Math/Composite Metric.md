
Pendo talks about their composite metric "Product Engagement Score"

![](hIgmzN5.png)

Derived from:
$\text{PES} = \left( \frac{\text{Adoption} + \text{Stickiness} + \text{Growth}}{3} \right) \times 100$


^Derived from:
1. Core Event is defined as the important feature (minor features ignored):

$\text{Adoption} = \left( \frac{\text{Average number of Core Events adopted across all active [visitors or accounts]}}{\text{Total number of Core Events}} \right) \times 100$

$\text{Stickiness} = \left( \frac{\text{Average [daily or weekly] active [visitors or accounts]}}{\text{Average [weekly or monthly] active [visitors or accounts]}} \right) \times 100$

$\text{Growth} = \frac{\text{Total number of new users + recovered users}}{\text{The number of dropped users}}$