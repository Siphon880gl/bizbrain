The conversion rate is calculated as the number of appointments divided by the number of emails sent, expressed as a percentage:

$\text{Conversion Rate} = \left(\frac{\text{Appointments}}{\text{Emails Sent}}\right) \times 100$

In the **current state**:
- 10 appointments generated from 10,000 emails:

$\text{Conversion Rate} = \left(\frac{100}{10,000}\right) \times 100 = 0.01 = 1\%$

This confirms that the current conversion rate is **1%**.

If Superwave scales up to 50,000 emails per month and maintains the same conversion rate of **1%**, it would generate:
$50,000 \times 0.01 = 500 \text{ appointments per month.}$

If the conversion rate of appointment to closing is 20%, then that's
$\$500 \times .20 = 100 \text{ appointments closed per month}$