Most basic are Churn and Retention  

There's churn rate and retention rate. Tracking and monitoring them are keys to meeting the goal (eg. 0% churn and 100% retention)

- **Churn rate**: number of customers leaving. Businesses aim for 0% churn. specifically, calculates the percentage of customers the business loses
  
  `$\text{Churn Rate} = \frac{\text{Number of Customers Lost During Period}}{\text{Total Customers at Start of Period}} \times 100$`

- **Retention rate**: number of customers staying. Businesses aim for 100% retention. specifically, calculates the percent of customers the business keeps
  
  `@\text{Retention Rate} = \frac{\text{Number of Customers at End of Period - New Customers Acquired During Period}}{\text{Total Customers at Start of Period}} \times 100@`

^ The reason why this works is that you're figuring out if you account for new customers, then whether or not there's a drop in customers between the start of the period and the end of the report

What other metrics in SaaS I should track or report? Investors want to look at it to see if their investment has return or it’s worth investing more. Business strategists want to look at it to determine if business improving or doing well or new strategies are working.  

- **Monthly Recurring Revenue (MRR)** and **Annual Recurring Revenue (ARR)**: These metrics provide a steady revenue baseline and growth trends. They’re crucial for understanding the company’s revenue health.
- **Customer Acquisition Cost (CAC)**: This is the cost of acquiring a new customer and is critical for evaluating the effectiveness of marketing and sales spending.
- **Customer Lifetime Value (LTV)**: A measure of the total revenue generated from a customer over their lifetime. Ideally, LTV should be significantly higher than CAC to ensure sustainable growth.
- **LTV Ratio**: This ratio compares the LTV to CAC, and a good benchmark is around 3:1. If it’s too low, you might need to reduce acquisition costs or improve retention; if too high, you might be underinvesting in growth.

- Business math thinking:
  You want LTV (value) to go up over time. LTV over CAC (Or LTV divided by CAC) creates the ratio. With the numerator increasing and the denominator decreasing, you want a large LTV ratio:
	![](zVNXAGs.png)
	^ Above from https://www.owox.com/blog/articles/interpreting-ltv-cac-ratio-what-the-numbers-reveal/

- **Net Revenue Retention (NRR)** and **Gross Revenue Retention (GRR)**: NRR shows growth from existing customers, accounting for upsells, downsells, and churn. GRR, excluding upsells, shows how well you’re keeping customers.
- **Average Revenue per User (ARPU)**: This measures how much revenue each customer generates on average, useful for tracking revenue growth per customer segment.
- **Expansion Revenue**: Revenue from upsells, cross-sells, and add-ons among existing customers. High expansion revenue signals strong product adoption and customer satisfaction.
- **Monthly Active Users (MAU)** and **Daily Active Users (DAU)**: These engagement metrics show user activity and help in understanding product stickiness.
- **Time to Value (TTV)**: Measures how quickly new customers experience the value of your product. Faster TTV usually correlates with higher customer satisfaction and retention.
- **Customer Satisfaction (CSAT) and Net Promoter Score (NPS)**: These are qualitative metrics that capture customer sentiment and loyalty, giving insights into potential retention and referrals.