Aka Get Started

A mathematical principle in business analysis is Orders of Magnitude.

This is when mathematical inputs scale up to very large outputs

Common business analysis and strategizing discussions involving orders of magnitudes are:
- When planning mass cold emailing
	- Eg. By purchasing 10 domains and setting up 3 email inboxes (3 email addresses) per domain, and if Gmail rules are still at 30 emails a day, you can increase your sending capacity to 900 emails daily ($10 \times 3 \times 30$). 
	- To take it further, in 30 days, that's 27k emails. With a conversion rate of 1%, that's 270 appointments/month. If a closing conversion rate of 20%, that's 54 closed appointments a month.
- When predicting profits
	- Eg. 100 real estate agents who pay for $75/monthly subscription. Our profit would be... $7500). An important metrics investors and stakeholders want to know is annual recurring profit, so then you multiply by 12 and get $90k/year


---

In the case of: "By purchasing 10 domains and setting up 3 email inboxes (3 email addresses) per domain, and if Gmail rules are still at 30 emails a day, you can increase your sending capacity to 900 emails daily ($10 \times 3 \times 30$)."

First rule principles:
- "Multiplying domains to 3 email inboxes per domain, etc... " 
	- You have to know what metric to multiply to what metric. In some math problems, you need to know what divide into what.
- "if Gmail rules are still at 30 emails a day, you can increase your sending capacity to 900 emails daily ($10 \times 3 \times 30$).
	- You should be able to think 10 and 30 are two 0's, then you can quickly figure out the magnitude is in the 100's, such as the result 900.
	- This applies to strategizing pricing packages and predicting profits too.

---


Under **Business Analytics and Forecasting**, this type of problem involves

1. **Unit Economics**:
    - Let's say you're calculating the performance metrics of your business by analyzing the inputs (emails sent) and outputs (appointments generated).
    - The ratio of **emails to appointments** is a key performance indicator (KPI) in this scenario.
2. **Scalability Analysis**:
    - Estimating future performance based on current results and assumptions about scaling operations (e.g., moving from 20 to 100 mailboxes).
3. **Proportional Scaling**:
    - Using proportional relationships to predict outcomes, assuming consistent efficiency (linear relationships like 1,000 emails → 1 appointment).
4. **Conversion Rate Analysis**:
    - Evaluating the percentage or ratio of conversions (appointments) relative to total efforts (emails sent).
5. **Optimization and Efficiency Modeling**:
    - Applying principles to maximize efficiency and predict outcomes under varying conditions.

In essence, you're leveraging **business modeling and forecasting** to understand and predict how scaling up resources (mailboxes, emails sent) will impact results (appointments).

---

## Segueways
- If advanced techniques or probabilistic factors come into play, this could also involve **statistical analysis** or **predictive modeling**.
- The ratio of **emails to appointments** is a key performance indicator (KPI) in this scenario.