
Notes about: [https://makealivingwriting.com/best-seo-websites/](https://makealivingwriting.com/best-seo-websites/)


- **Definition**: Focuses on improving the website's technical aspects to enhance overall performance and user experience.
- **Key Elements**:
    - **Page Speed**: Affects user experience and ranking.
    - **User Experience (UX)**: The overall experience of a user interacting with the website.
    - **robots.txt File**:
        - Controls which pages are crawled by search engines.
        - Located at the root directory of a website (e.g., `example.com/robots.txt`).

---

#### Robots.txt File

This code tells search engines to crawl all parts of the website except the `/private/` directory.

```
User-agent: * Disallow: /private/ Allow: /
```


