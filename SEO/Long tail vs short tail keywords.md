In the context of Search Engine Optimization (SEO), keywords can be broadly categorized into two types: short tail keywords and long tail keywords. Both play crucial roles in SEO strategy, but they serve different purposes and offer distinct advantages and disadvantages.

1. **Short Tail Keywords**: 
   - Aka: **head tail**
   - **Definition**: These are broad and general keywords, usually consisting of one to three words. They are often very generic and attract a high volume of search traffic.
   - **Example**: "shoes," "digital camera," or "coffee shop."
   - **Example:** "condo"
   - **Advantages**:
     - **High Search Volume**: They attract a large number of searches because they are broad and cover a wide range of topics.
     - **Brand Visibility**: Ranking for short tail keywords can significantly increase brand visibility and recognition due to the high search volume.
   - **Disadvantages**:
     - **High Competition**: Because they are so broad, many businesses compete for these keywords, making it challenging to rank high in search results.
     - **Lower Conversion Rates**: Short tail keywords are less specific, so while they might attract more traffic, they often have lower conversion rates compared to more specific queries.

3. **Long Tail Keywords**:
   - **Definition**: These are more specific keyword phrases that are usually longer than three words. They are often used by **searchers who are closer to making a decision or purchase**.
   - **Example**: "men's leather shoes size 11," "best digital camera for beginners," or "organic coffee shop in downtown Chicago."
   - **Example:** "pasadena new condos for sell"
   - **Advantages**:
     - **Lower Competition**: Since they are more specific, there's generally less competition for long tail keywords, making it easier to rank for them.
     - **Higher Conversion Rates**: Users searching for long tail keywords are often looking for something very specific, which means they're closer to making a purchase or taking an action, leading to higher conversion rates.
     - **Cost-Effective**: For paid search campaigns, long tail keywords often have a lower cost per click due to the lower competition.
   - **Disadvantages**:
     - **Lower Search Volume**: Long tail keywords have fewer searches compared to short tail keywords, which could result in lower traffic.
     - **More Research and Content Creation**: To effectively target long tail keywords, businesses need to conduct thorough research and create more specific content, which can be resource-intensive.

In a balanced SEO strategy, both short tail and long tail keywords are essential. Short tail keywords help in gaining visibility and broad traffic, while long tail keywords are crucial for targeting specific audiences and improving conversion rates. Effective SEO involves a mix of targeting high-volume, competitive keywords (short tail) and specific, niche phrases (long tail) to cater to various stages of the user's search intent and journey.