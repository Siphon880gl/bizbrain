Notes about: [https://makealivingwriting.com/best-seo-websites/](https://makealivingwriting.com/best-seo-websites/)

- **Definition**: On-page SEO refers to optimizing webpages to rank higher in search engines through content.
- **Key Elements**:
    - **Header Tags (H-tags)**: Organize content and help search engines understand the structure of the page.
    - **Page Title**: The title of the page that appears in search results.
    - **URL**: Should be concise and include the main keyword.
    - **Meta Description**: A brief description of the page content that appears in search results.
- **Keyword Usage**:
    - Main keyphrase should be 3-4 words, used in H-tags and throughout the page.
    - Avoid outdated practices like keyword stuffing.
- **Out of practice**
	- Keyword density
		- What is: 1-2 keyword for every 100 words of copy for about 1-2% keyword density
		- Google confirmed is not a significant SEO ranking factor.

---

#### Meta Description HTML Tag

This HTML tag provides a brief summary of the webpage's content for search engine results

```
<meta name="description" content="This is an example of a meta description for SEO purposes.">
```
