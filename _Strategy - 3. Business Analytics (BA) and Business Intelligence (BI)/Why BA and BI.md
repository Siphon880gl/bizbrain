
For investors, for internal founders.

If there's no growth, then it's clear we have to work on growing the app, if it's relevant at the current phase of the company. Everyone has their own login and can see live data that's easy to comprehend (no navigating databases) at any time. The live data could be based on the database and/or based on user behaviors (marketing analytics).