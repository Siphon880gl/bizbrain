
Moved to coding notes: wengindustries.com/app/devbrain/?open=_What%20is%20BA%20and%20BI

Rationale for moving to coding notes instead of keeping it at tech business notes: Requires specialized coding knowledge, so fits there better.

To avoid fragmenting the notes between two notebooks, all BA/BI (whether it's coding or how to use their platform) are in the coding notebook.