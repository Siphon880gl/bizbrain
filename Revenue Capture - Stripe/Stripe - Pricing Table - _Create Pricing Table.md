Aka: Three tier pricing model
Aka: Good-Better-Best
Aka: Pricing table

## Instructions

![](vZ1y0v4.png)


At [https://dashboard.stripe.com,](https://dashboard.stripe.com,) go to More → Product catalog

We review if you have the requirements satisfied (three products, each product with at least two recurring prices)

- You should create three products named after the three tiers (eg. Good, Better, Best). Your pricing table
- For each product, you can add month pricing. Then add a yearly pricing. Remember that a product can have multiple prices. Refer to [[Stripe - Product - Create a product]] and [[Stripe - Product - Create a price for the product]].
  
Naming ideas:
- Examples of a “good/better/best” product naming hierarchy: Bronze, Silver, Gold / Basic, Premium, Ultra / Classic, Prime, Platinum.
- Basic, Advanced, Deluxe  
- Lite Plus Premium
  [https://getjobber.com/academy/good-better-best-pricing-examples/](https://getjobber.com/academy/good-better-best-pricing-examples/)

If in Test Mode, when you’re ready to charge users, you can copy to Live Mode
![](aHVFcJc.png)

Now we can create the pricing table or modify an existing one. To create a pricing table. See that the “Pricing catalog” section of Stripe actually has a “Pricing Tables” tab. Go to the tab
![](MqOTZsj.png)
^And at the top right is the purple button “Create pricing table”

Start assigning products and designing your pricing table
![](r3TldMB.png)

Search Product for one of the columns of the pricing table:
![](l3lxN3H.png)

You select by a price under a product.

![](kVWHuvA.png)


Add another price because you want to offer monthly rate as well as a cheaper yearly rate.

![](Omiztvi.png)

Then continue adding more products on the same page:
![](BZxOn0k.png)

Notice when user clicks a button, they can sign up by email. If you’re integrating with custom coding or an app, you may want to look into asking your Stripe Developer to have web hooks to trigger new app account creation on new subscription from email address and other related logic:
![](jnIhbY7.png)

Customize your theme colors:
![](b3NZQw3.png)

---

## Niceties: Upsells

You may want to consider adding an upsell when a customer is at the checkout page for a monthly plan. That will encourage them to pay upfront the yearly charge. Refer to [[Stripe - Checkout - Upsell]].

---

## Niceties: Feature Lists

You may want to add or modify the feature list for each product. See the “this includes” for each tier. Refer to [[Stripe - Pricing Table - Feature Lists]]

![](ciMfKxv.png)

---

Then paste the embed code into your Wordpress or custom coded website. 

On Wordpress, you may need to adjust the CSS around the UI elements to make them look to your preferences. For example:
```
<style>    
body {    
  background: white !important;    
}    
stripe-pricing-table {    
  margin: 0 !important;    
  padding: 0 !important;    
  display: block !important;    
  width: 100vw !important;    
}    
.container.main-content {    
  margin: 0 !important;    
  padding: 0 !important;    
}    
.container-wrap {    
  background: white;    
}    
</style>
```