Feature list at each subscription on your pricing table can be modified as followed:  

Product Catalog → Pricing Table → Select specific pricing table → Edit Pricing Table → On the left sidebar choose “Edit Product”

![](cxJB75n.png)
