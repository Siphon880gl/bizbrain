## User flow
Customer clicks a pricing table and it goes to the checkout page.

Let's say they choose a monthly payment from a pricing table, then they see an upsell "Save -$XXX" for switching. If the user agrees by clicking the toggle button, it would change the checkout page's confirmation from monthly to yearly (no need of opening another page).

## Stripe's Upsell
Stripe considers upsell as discounting time or price. Stripe does not consider upselling as selling more features for a higher price. Their definition when you hover a mouse cursor over the (i) at Upsell section:

"Upsells are currently available for prices with the same currency, longer billing periods, and an annualized price that is less than or equal to the annualized base price."

## Limitations
Does not work for times one payments.

How it looks:
![](ouJToT0.png)

Zoomed in:
![](mcZKL5l.png)

After user clicks “Save $240”, it switches to yearly:
![](mgiXVOF.png)

## How to add upsell

Product catalog → Click specific Product → Click specific Price → Upsells section → Select another price


Note doesnt work for times one prices. 

Note if you stayed on the product page and have gone to “Edit Price” on a price instead of visiting the price page, that “Edit Price” side panel that appears DOES NOT have the upsell option.

![](RZOJcuW.png)
