
While in test mode you can still test how it is as if you were a customer at checkout price by using test credit cards (more later)
![](460aIq1.png)

Your products/payment links/transactions/customers/invoices etc will be sandboxed for Test mode. The product, billing, and customer information in Test Mode doesn't spill over or share with those in the Live Mode, and vice versa.

You will probably want to use test credit cards while testing at the customer side "paying" for a product. Refer to: 