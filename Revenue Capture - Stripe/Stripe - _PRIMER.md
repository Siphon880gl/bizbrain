Aka: Get Started

**How to use**: Recommend you open a screen-persistent Table of Contents so you can navigate this document more easily.

---

## Introduction
### Why Stripe
Stripe is a popular choice for payment processing for several reasons:

1. **Credit Card Handling & PCI Compliance**: Stripe takes care of PCI DSS (Payment Card Industry Data Security Standard) compliance, meaning you don’t have to handle sensitive payment information directly, reducing security risks.

2. **Multiple Payment Methods**: It supports a wide range of payment methods, including credit cards, digital wallets (e.g., Apple Pay, Google Pay), and local payment methods globally, helping businesses reach more customers.

3. **Customizable Embeds**: Stripe offers pre-built, aesthetically pleasing payment embeds that are easy to integrate, ensuring a smooth user experience without the need for heavy customization.

4. **Flexible Pricing Models**: Stripe accommodates various pricing models like one-time payments, subscriptions, usage-based billing, and dynamic pricing, which is great for businesses with different revenue models.

5. **API & Developer-Friendly**: Stripe’s API is designed to be straightforward and well-documented, making it easy for developers to integrate and scale payment solutions into web and mobile apps.

6. **Global Reach**: Stripe supports payments in multiple currencies and regions, enabling businesses to scale internationally.

These features make Stripe a strong solution for businesses looking for a secure, flexible, and developer-friendly payment platform.
  
### Fees

Stripe processing fee is 2.9% + 30 cents
- So out of 1 dollar, it charges .33
- Out of 100 dollars, it charges $3.20
- For $75, they receive $2.48

### Glance on Stripe
Stripe has a quick overview that you will be setting up a Stripe Account, accepting your first payment, and managing payouts. My tutorials will end up going through  all the major steps. But read this first to get an overview:
https://support.stripe.com/topics/getting-started

---

## Setup

1. Visit [https://dashboard.stripe.com/](https://dashboard.stripe.com/)
2. Create account. it’s free.
3. Verify your email address
4. You can activate payments or jump to Test Mode where real money wont be charged. While in test mode you can still test how it is as if you were a customer at checkout price by using test credit cards (more later). 
	- Recommended: Test Mode
	   How: [[Stripe - _Test Mode]]![](460aIq1.png)
	- Or: Activate Payments with [[Stripe - _Setup - Activate Payments]]

---

## Recommended Use

Follow these steps to build your products and prices for your business, referring to the adjacent tutorials. If you do not have a business or business client currently, plan out a hypothetical business and products (You'd have to come up with a hypothetical business, its products, and its prices on paper):

- Design your products bearing in mind how users see them
- Design your prices inside each product bearing in mind how user sees them and what you will toggle on for holidays and specials and what upsells there are

- Get your ui embeds or use Stripe Elements API. And publish them to your website or app on a hidden URL or development environment.  
- Pretend to be the customer and pay from your website or app
- Test your checkout page and design if necessary such as limiting quantity and having upsell option (switch monthly to yearly to save $X) on the checkout page
- You’ll want to test that new customers are created when you paid with an email address


----

## Manage Payments

### Orientate Yourself

Orientate yourself to these screens because you'll be managing customer payments across these screens
- Note these are in Test Mode with test credit cards - notice master card ending in 4444.
- You usually identify the customer by their email address that is a possible input at the checkout page.
  
![](eChx1g3.png)


![](X2A9cY3.png)

Then depending if you have subscriptions or you charge them by sending invoices (for higher ticket items or B2B):
![](xTZDYUl.png)

![](Kq9lsNL.png)

The item on the left sidebar would be named the same as the page heading. If you don't see the menu item, you have to click "... More" to find and add it to the Shortcuts area in the sidebar.

---

## PATHWAYS - Live Mode

When you're ready to charge real customers and you've been using the sandboxed Test Mode, you can copy the products into Live Mode, and then turn off Test Mode. These steps are at [[Stripe - _Test Mode - Switch to Live Mode]]. Another requirement is that you've activated payments by providing your business information and bank information, referring to [[Stripe - _Setup - Activate Payments]].