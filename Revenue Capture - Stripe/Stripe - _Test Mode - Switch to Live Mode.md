**Where we are:**
You've just been using Test Mode to test products and charging. It's time to charge real customers, so you're going to turn off test mode. You have to copy the products into Live Mode since Test Mode is a sandboxed version of your account:

1. Copy over your products which will include their prices:

![](90OoegM.png)



2. Toggle off "Test mode":
![](7jc2GHA.png)


3. Navigate to your products (Sidebar "Product catalog") while in Live Mode to see the copied over products.