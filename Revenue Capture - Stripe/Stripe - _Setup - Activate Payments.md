To accept payments right away, add the required information to your business profile. Or start by exploring Stripe's features to find the right fit for you.

Add business information: https://dashboard.stripe.com/account/onboarding

Talk to your founders or higher ups about being added to the business owner list. It’s for their regulatory requirements

Talk to them about the bank account. If bootstrapping the business, it’ll be someone’s bank account temporarily before coming up with a joint business account. It’ll OAuth2 login to an online bank account

Stripe gets your 

- Available Balance
- Current Balance
- Interest Earned YTD
- Interest Paid Last Year
- **If you choose manual, you can be specific about the check account. because you’re entering route and check. You can create a check account just for this**  
- **Employer Identification Number (EIN)**  
	- **Can be your SSN or taxpayer identification number for now then ask higherups for EIN**  
	- **Stripe also requires an appropriate EIN for receiving payment. In the mean time I can place my social security number which will make the company name that appears in checkout as my legal name and that's what people will see in the checkout. So we will need a DBA once we start charging so Stripe will allow a business name so the checkout looks more official  
- **You can also skip adding a bank for now clicking Continue**  
- **If business is Individual, you can enter your name and SSN**  
- Check no errors at payouts page (aka Balances page). It will tell you at this page:
  [https://dashboard.stripe.com/payouts](https://dashboard.stripe.com/payouts)
  
  For example:
  ![](Cuu9Fvr.png)

---

When successful it goes to https://dashboard.stripe.com/account/activated
![](2buXTu2.png)
