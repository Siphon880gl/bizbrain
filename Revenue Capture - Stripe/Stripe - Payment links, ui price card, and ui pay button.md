Under Shortcuts or “...More” at sidebar, go for Payment Links.
![](ZhjX8vf.png)

Payment Links can create card, button, or url to checkout page. When user clicks the button on the card or clicks the button, it would go to the checkout page.

These are the elements that can be built:
- UI price card
  ![](Xo3tiRN.png)

- UI payment button:
  ![](rcsgvNY.png)

- Link to checkout page:
  ![](CWJfgyy.png)

---

**How to get checkout URL**

URL is gotten here after clicking "Payment Links":
![](Ju4q3EY.png)


**How to create**

UI Card or Button is created after clicking "Pay button"
![](g9GzB30.png)

Then you choose which element:
![](dTOJy78.png)

Then copy the code it provides you.

**How to embed**
You can copy the js embed code to a raw JS code block in Wordpress or paste into custom coded website

On Wordpress, you may need to adjust the CSS around the UI elements to make them look to your preferences. For example (this snippet includes pricing table too though):
```
<style>  
body {  
  background: white !important;  
}  
stripe-pricing-table {  
  margin: 0 !important;  
  padding: 0 !important;  
  display: block !important;  
  width: 100vw !important;  
}  
.container.main-content {  
  margin: 0 !important;  
  padding: 0 !important;  
}  
.container-wrap {  
  background: white;  
}  
</style>
```