You probably want products and prices from Test Mode which is a sandboxed version of Stripe. To turn on Test Mode, refer to [[Stripe - _Test Mode]]

Stripe offers a list of test credit card numbers, CVV (card verification value), and expiration dates:
[https://docs.stripe.com/testing#cards](https://docs.stripe.com/testing#cards)  

  
Here's an example:
Mastercard
Number: 5555555555554444
CVV: Any 3 digits
Exp: Any future date