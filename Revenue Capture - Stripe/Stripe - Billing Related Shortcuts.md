
This is a page of shortcuts to billing related features on Stripe which includes:
- Creating invoice page for customers
- Creating subscription for a customer ID
- Getting a link to checkout page 
- Checklist to getting your Stripe ready to charge customers

From the sidebar open "Billing overview"
![](HQDDpWv.png)

Or visit the link: https://dashboard.stripe.com/test/billing/setup

Stripe describes that page as: "Automate and manage recurring and one-off payments"