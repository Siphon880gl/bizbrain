
Quick guide by Weng

Heres the direct link to Google Analytics dashboard:
https://analytics.google.com/analytics/web/

Go bookmark the webpage because Google Search will send you to support documents and marketing division instead.

## Create new dashboard for tracking a website or app

Home → Start collecting data for your website or app → Web / Android /iOS 

![](0PsYECT.png)

## Setup

First it will ask for your web url

Along the way it might ask for your business info in the middle, including how many headcounts.

Along the way it’ll talk about Installation instructions or tag help. It has streamlined ways to add analytics, or you can switch tab to Manual:

![](HyFEkoG.png)

If Wordpress, you add the code snippet to header.php at wp-content/themes/THEME_NAME/header.php

- Avoid adding between a pair of `<?php`  and `?>` . If you don’t want to insert code manually in Wordpress, get the plugin MonsterInsights.

If React, you add to index.html (not the one in build or dist), then you’ll build it

If traditional webpage, you add to your index.html or whatever applicable webpage file.

---

## European

**Have European customers? Need to accept their agreement:**

Account -> Account Details "I accept the Data Processing Terms as required by GDPR"